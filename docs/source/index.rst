.. PyFABIL documentation master file, created by
   sphinx-quickstart on Wed Aug  9 10:18:17 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyFABIL's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Overview:
   
   overview
 
.. toctree::
   :maxdepth: 3
   :caption: Plugins:

   plugins
 