forty_g_xg_core
================

Overview
----------

configures the 40g version of the udp core (used by default on both TPMs)

Python Class & Methods Index
---------------------------------

.. automodule:: pyfabil.plugins.tpm.forty_g_xg_core
    :members:
    :undoc-members: