beamf_simple
==============

Overview
---------

simpler version of tile beamformer, configuations including beam pointing

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm.beamf_simple
    :members:
    :undoc-members: