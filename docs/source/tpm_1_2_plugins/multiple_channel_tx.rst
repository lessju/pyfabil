multiple_channel_tx
====================

Overview
----------

configures the multiple channelizer LMC.

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.multiple_channel_tx
    :members:
    :undoc-members: