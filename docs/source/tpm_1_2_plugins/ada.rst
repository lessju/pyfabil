ada
============

Overview
-----------

Controls the artifical data amplifier, only in tpm 1.2 and usually not fitted.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.ada
    :members:
    :undoc-members: