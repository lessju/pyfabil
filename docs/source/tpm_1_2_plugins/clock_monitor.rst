clock_monitor
===============

Overview
-------------

configure or enable/disable clock monitoring

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.clock_monitor
    :members:
    :undoc-members: