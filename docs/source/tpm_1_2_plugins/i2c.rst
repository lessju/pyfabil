i2c
====

Overview
----------

read and write to all I2C devices, e.g. tempreture sensor, eeprom, qsfp control, front panel led

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.i2c
    :members:
    :undoc-members: