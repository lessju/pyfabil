preadu
========

Overview
---------

initializes the preADU and controls the preADU attenuation. Also on TPM 1.2 the preADU filters can be controlled.

Python Class & Methods Index
--------------------------------

.. automodule:: pyfabil.plugins.tpm.preadu
    :members:
    :undoc-members: