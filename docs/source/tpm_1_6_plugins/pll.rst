pll
=====

Overview
---------

configure the PLL on the TPM board.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.pll
    :members:
    :undoc-members: