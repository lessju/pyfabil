eep
====

Overview
---------

configure, read and write the eep rom

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.eep
    :members:
    :undoc-members: