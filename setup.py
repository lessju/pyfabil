from setuptools import setup, find_packages
import re


def get_property(property, project='pyfabil'):
    result = re.search(r'{}\s*=\s*[\'"]([^\'"]*)[\'"]'.format(property), open(project + '/__init__.py').read())
    return result.group(1)

setup(
    name='pyfabil',
    version=get_property('__version__', 'pyfabil'),
    packages=find_packages(),
    url='https://bitbucket.org/lessju/pyfabil',
    license='GPLv3',
    author='Alessio Magro',
    author_email='alessio.magro@um.edu.mt',
    description='A general Python package for interfacing with FPGA boards',
    install_requires=['enum34','wheel','future','numpy'],
)
