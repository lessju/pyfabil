__author__ = 'Alessio Magro'
__version__ = '1.2.0'

from pyfabil.base.definitions import *
from pyfabil.boards.tpm_generic import TPMGeneric
from pyfabil.boards.tpm_1_6 import TPM_1_6
from pyfabil.boards.tpm import TPM