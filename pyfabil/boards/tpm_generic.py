import pyfabil.boards.tpm_hw_definitions as tpm_hw_definitions
from pyfabil.base.definitions import *
from pyfabil.boards.fpgaboard import FPGABoard, DeviceNames
from pyfabil.protocols.ucp import UCP


class TPMGeneric(FPGABoard):
    """ FPGABoard subclass for communicating with a TPM board """

    def __init__(self, **kwargs):
        """ Class constructor """
        kwargs['fpgaBoard'] = BoardMake.TpmBoard
        kwargs['protocol'] = UCP

        # Call superclass initialiser
        super(TPMGeneric, self).__init__(**kwargs)

    def get_tpm_version(self, ip, port, **kwargs):
        """ Overload connect method
        :param fsample: Sampling rate
        :param ip: IP address to connect to
        :param port: Port to connect to
        """

        super(TPMGeneric, self).connect(ip, port, **kwargs)
        try:
            _tpm_version = self.read_address(tpm_hw_definitions.CPLD_MAGIC_OFFSET)
        except:
            raise LibraryError("Could not reach TPM with address {}".format(ip))

        if _tpm_version == tpm_hw_definitions.CPLD_MAGIC_TPM_V1_2:
            return "tpm_v1_2"
        elif _tpm_version == tpm_hw_definitions.CPLD_MAGIC_TPM_V1_5:
            return "tpm_v1_5"
        else:
            raise LibraryError("TPM CPLD_MAGIC mismatch. Got 0x%x." %_tpm_version)


# ------------------------------------------------ Tile Class ---------------------------------------

if __name__ == "__main__":
    tpm = TPMGeneric()
    tpm.get_tpm_version(ip="10.0.10.13", port=10000)
