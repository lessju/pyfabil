from __future__ import division
from builtins import str
from builtins import hex
from builtins import range
from builtins import int
from pathlib import Path
import binascii
import logging
import math
import socket
import struct
import time
import zlib
from math import ceil
from time import sleep

import pyfabil.boards.tpm_hw_definitions as tpm_hw_definitions
from pyfabil.base.definitions import *
from pyfabil.boards.fpgaboard import FPGABoard, DeviceNames
from pyfabil.protocols.ucp import UCP


class TPM(FPGABoard):
    """ FPGABoard subclass for communicating with a TPM board """

    def __init__(self, **kwargs):
        """ Class constructor """
        kwargs['fpgaBoard'] = BoardMake.TpmBoard
        kwargs['protocol'] = UCP

        # Set hardcoded CPLD xml offset address
        self._cpld_xml_offset = tpm_hw_definitions.CPLD_XML_OFFSET
        self._cpld_magic = tpm_hw_definitions.CPLD_MAGIC_TPM_V1_2

        # Hardware revision
        self.hw_rev = tpm_hw_definitions.TPM_HW_REV_1_2

        # Flag to check whether XML file is loaded
        self._xml_loaded = False

        # Initialise variables
        self._simulator = False
        self._initialise = False
        self._enable_ada = False
        self.adas_enabled = False
        self._enable_adc = True
        self._disabled_adc = []

        # CPLD SelectMap Registers
        self._xil_registers = [0x50000004, 0x50000008]
        self._icap_register = [0x8000000C, 0x80000010]
        self._global_register = 0x50000000
        self._c2c_stream_enable = 0x30000018
        self.download_firmware_required_cpld_version = tpm_hw_definitions.CPLD_FW_VER_REQUIRED_FOR_FPGA_DOWNLOAD_TPM_V1_2

        # Call superclass initialiser
        super(TPM, self).__init__(**kwargs)

    def connect(self, ip, port,
                fsample=800e6,
                ddc=False,
                fddc=0.0,
                adc_clock_divider=1,
                mono_channel_14_bit=False,
                mono_channel_sel=0,
                adc_low_bitrate=False, **kwargs):
        """ Overload connect method
        :param fsample: Sampling rate
        :param ip: IP address to connect to
        :param port: Port to connect to
        """

        # Check if we are simulating or not
        self._simulator = kwargs.get('simulator', False)

        # Check if we are initialising or not
        self._initialise = kwargs.get('initialise', False)

        # TPM might or might not have an ADA
        self._enable_ada = kwargs.get("enable_ada", False)

        # You can skip ADC enabling for test/debug purpose (default True to preserve back compatibility)
        self._enable_adc = kwargs.get("enable_adc", True)

        # List of disabled ADC, these will not be configured
        self._disabled_adc = kwargs.get("disabled_adc", [])

        # Call connect on super class
        super(TPM, self).connect(ip, port, **kwargs)

        # CPLD is assumed programmed
        self._programmed[Device.Board] = True

        # Try to read address 0x30000000 (CPLD version) check whether TPM is reachable
        try:
            self.read_address(tpm_hw_definitions.CPLD_VERSION_OFFSET)
        except:
            self.status[Device.Board] = Status.NetworkError
            self._connected = False
            raise LibraryError("Could not reach TPM with address {}".format(ip))

        # Check CPLD magic number
        _tmp = self.read_address(tpm_hw_definitions.CPLD_MAGIC_OFFSET)
        if _tmp != self._cpld_magic:
            raise LibraryError("TPM CPLD_MAGIC mismatch. Expected 0x%x, got 0x%x."%(self._cpld_magic,_tmp))

        # Check version (check whether loaded CPLD firmware supports XML files)
        version = self.read_address(tpm_hw_definitions.CPLD_VERSION_OFFSET)
        if version == tpm_hw_definitions.CPLD_FW_VER_BLACKLIST_FOR_XML_SUPPORT_TPM_V1_2:

            # CPLD version too old, does not have an XML file
            logging.warning("tpm.connect: CPLD version is very old, XML files not supported")

            # Only load CPLD plugin
            self.load_plugin("TpmCpld")

            return

        # Get XML file. Continue if error occurs while reading the XML file from the CPLD
        # as the failsafe CPLD bitfile does not contain the XML file
        try:
            cpld_xml = str(self._get_xml_file(self.read_address(self._cpld_xml_offset)))
            cpld_xml_ok = True
        except:
            logging.error("CPLD XML file read failed. Continuing without CPLD XML file...")
            cpld_xml_ok = False
        if cpld_xml_ok:
            cpld_xml = cpld_xml.replace('<?xml version="1.0" ?>', '')
            cpld_xml = "<node>\n{}\n</node>".format(cpld_xml)
            super(TPM, self).load_firmware(device=Device.Board, register_string=cpld_xml)
            self._xml_loaded = True
            logging.debug("tpm.connect: CPLD XML File Downloaded")

        # Load plugins
        self.load_plugins()

        # Initialise devices if required
        if not self._simulator and self._connected:
            if self._initialise:
                self._initialise_devices(fsample=fsample, ddc=ddc, fddc=fddc, adc_clock_divider=adc_clock_divider,
                                         mono_channel_14_bit=mono_channel_14_bit, mono_channel_sel=mono_channel_sel,
                                         adc_low_bitrate=adc_low_bitrate, disabled_adc=self._disabled_adc)

    def load_plugins(self):
        """ Load plugins to board instance """

        # Load the I2C plugin (does not require the board to be initialised)
        self.load_plugin("TpmI2c")

        # Load the CPLD plugin (does not require the board to be initialised)
        self.load_plugin("TpmCpld")

        # Load LASC chip plugin (does not require the board to be initialised)
        self.load_plugin("TpmLasc")

        # If not simulating, get firmware from board and initialise if required
        if not self._simulator and self._connected:

            # Load PREADU plugins
            self.load_plugin("PreAdu", preadu_id=0)
            self.load_plugin("PreAdu", preadu_id=1)

            # Try initialising the board (will check if FPGAs are programmed)
            if not self._initialise_board():
                return

            # Pre-load all required plugins. Board-level devices are loaded here
            [self.load_plugin("TpmFirmwareInformation", firmware=x) for x in range(1, 4)]

            # Load ADAs
            self.load_plugin("TpmAda")

            # Load ADCs
            [self.load_plugin("TpmAdc", adc_id=adc) for adc in
                ["adc0", "adc1", "adc2", "adc3", "adc4", "adc5", "adc6", "adc7",
                "adc8", "adc9", "adc10", "adc11", "adc12", "adc13", "adc14", "adc15"]]

            # Load PLL
            self.load_plugin("TpmPll")

            # Update firmware information
            if self.is_programmed():
                [info.update_information() for info in self.tpm_firmware_information]

    def get_firmware_list(self, device=Device.Board):
        """ Get list of downloaded firmware on TPM FLASH
        :param device: This should always be the board for the TPM
        :return: List of design name as well as version
        """

        # Got through all firmware information plugins and extract information
        firmware = []
        for plugin in self.tpm_firmware_information:
            # Update information
            plugin.update_information()

            # Check if design is valid:
            if plugin.get_design() is not None:
                firmware.append({'design': plugin.get_design(),
                                 'major': plugin.get_major_version(),
                                 'minor': plugin.get_minor_version()})
        # All done, return
        return firmware

    def smap_select_fpga(self, fpga_list):
        for n in fpga_list:
            self[self._xil_registers[n]] = 0x10

    def smap_deselect_fpga(self, fpga_list):
        for n in fpga_list:
            self[self._xil_registers[n]] = 0x0

    def erase_fpga(self,force=True):
        """ Erase FPGAs configuration memory """

        def smap_data_format(dat):
            def reverse_bit(num):
                result = 0
                for n in range(8):
                    result = (result << 1) + (num & 1)
                    num >>= 1
                return result

            smap_dat = 0
            for n in range(4):
                smap_dat |= reverse_bit(dat >> 8 * n) << 8 * n
            return smap_dat

        # Check if connected
        if not self.is_connected():
            raise LibraryError("TPM needs to be connected in order to program FPGAs")

        self[self._c2c_stream_enable] = 0x0  # Disable C2C stream

        # Temporary
        self[self._global_register] = 0x3

        # Erase FPGAs SRAM
        for fpga_id in range(len(self._xil_registers)):
            xil_register = self._xil_registers[fpga_id]

            logging.info("Erasing FPGA%d using PROG pin method" % fpga_id)
            self[xil_register] = 0x10  # Select FPGA
            self[self._global_register] = 0x0  # PROG = 0

            max_attempt = 10
            attempt = 0
            fpga_still_programmed = 0
            while self[xil_register] & 0x1 == 1:  # wait for INIT
                if attempt == max_attempt:
                    fpga_still_programmed = 1
                    break
                attempt += 1
                sleep(0.1)
            self[self._global_register] = 0x3
            while self[xil_register] & 0x1 == 0:  # wait for INIT
                if attempt == max_attempt:
                   fpga_still_programmed = 1
                   break
                attempt += 1
                sleep(0.1)

            # If FPGA erase with PROG pin method timed out, try with ICAP3 method
            if fpga_still_programmed == 1:
                logging.info("Erasing FPGA%d using PROG pin method failed, trying ICAP3 method" % fpga_id)
                self[self._global_register] = 0x2
                ba = self[self._icap_register[fpga_id]]
                self.write_address(ba + 0x8, smap_data_format(0xFFFFFFFF), retry=False)  # Dummy word
                self.write_address(ba + 0x8, smap_data_format(0xAA995566), retry=False)  # Sync word
                self.write_address(ba + 0x8, smap_data_format(0x20000000), retry=False)  # Type 1 NOOP
                self.write_address(ba + 0x8, smap_data_format(0x30020001),
                                   retry=False)  # Type 1 Write 1 words to WBSTAR
                self.write_address(ba + 0x8, smap_data_format(0x00000000),
                                   retry=False)  # Warm boot start address (Load the desired address)
                self.write_address(ba + 0x8, smap_data_format(0x30008001), retry=False)  # Type 1 Write 1 words to CMD
                self.write_address(ba + 0x8, smap_data_format(0x0000000F), retry=False)  # IPROG command
                self.write_address(ba + 0x8, smap_data_format(0x20000000), retry=False)  # Type 1 NOOP

                attempt = 0
                self[self._global_register] = 0x3
                while self[xil_register] & 0x1 == 0:  # wait for INIT high
                    if attempt == max_attempt:
                        break
                    attempt += 1
                    sleep(0.1)
                self[xil_register] = 0x0  # deselect FPGA

        self[self._global_register] = 0x3

    def calibrate_fpga_to_cpld(self):
        """ Calibrate communication between FPGAs and CPLD """
        self.set_c2c_calibration(True)

        # Disable c2c streaming
        self[self._c2c_stream_enable] = 0x0
        while self[self._c2c_stream_enable] != 0x0:
            time.sleep(0.01)

        # PLL in CPLD calibrated in 64 steps (done twice)
        devices = ["fpga1", "fpga2"]

        for f in devices:
            if f == 'fpga1':
                m = 0
                phasesel = 0
            else:
                m = 1
                phasesel = 1
            lo = -1
            this_error = -1
            mask = 0x1 << (4 + m)
            for n in range(128):
                time.sleep(0.01)
                previous_error = this_error
                this_error = (self[0x30000040] & mask) >> (4 + m)

                if this_error == 0 and (previous_error == 1 or previous_error == 2 or previous_error == 3) and lo == -1:
                    lo = n

                if (this_error == 1 or this_error == 2 or this_error == 3) and previous_error == 0 and lo != -1:

                    k = (((n - 1) - lo) // 2) + 1
                    for x in range(k):
                        self[0x30000028] = 0x010 + (phasesel << 8)
                        self[0x30000028] = 0x011 + (phasesel << 8)
                        self[0x30000028] = 0x010 + (phasesel << 8)
                        time.sleep(0.02)

                    logging.info("%s to CPLD calibrated", f.upper())
                    logging.info("%s to CPLD. Start phase: %i, Stop phase %i " % (f.upper(), lo, n))

                    # Disable calibration pattern transmission
                    if m == 0:
                        break
                    else:
                        self.set_c2c_calibration(False)
                        return

                # Advancing PLL phase
                self[0x30000028] = 0x000 + (phasesel << 8)
                self[0x30000028] = 0x001 + (phasesel << 8)
                self[0x30000028] = 0x000 + (phasesel << 8)
                time.sleep(0.01)

        logging.fatal("Could not calibrate FPGA to CPLD streaming")

    def set_c2c_calibration(self, enable=True):
        if enable:
            wr = 0x5A01
        else:
            wr = 0x1
        # self['board.regfile.c2c_ctrl.mm_read_stream'] = 0
        self[0x3000002C] = 1
        for ba in [0x0, 0x10000000]:
            try:
                self.write_address(ba + 0x90, wr, retry=False)
                # self[ba + 0x90] = 0x5A00
            except:
                pass

    def download_firmware(self, device, bitfile):
        """ Download bitfile to FPGA
        :param device: FPGA to download bitfile
        :param bitfile: Bitfile to download
        """
        # Check if connected
        if self.status[Device.Board] != Status.OK:
            raise LibraryError("TPM needs to be connected in order to program FPGAs")

        required_cpld_version = self.download_firmware_required_cpld_version
        cpld_version = self[tpm_hw_definitions.CPLD_VERSION_OFFSET]
        if cpld_version < required_cpld_version or cpld_version & 0xF0 == 0xB0:
            logging.error("CPLD firmware version is too old. Required version is " + hex(required_cpld_version))
            raise LibraryError("CPLD firmware version is too old. Required version is " + hex(required_cpld_version))

        # Sanity checks on provided bitfile (exists and size)
        f = Path(bitfile)
        if not f.exists():
            raise LibraryError("Provided bitfile ({}) does not exist".format(bitfile))
        elif f.stat().st_size < 1024:
            raise LibraryError("Provided bitfile ({}) is too small ({} bytes)".format(bitfile, f.stat().st_size))

        # Disable C2C stream
        self[self._c2c_stream_enable] = 0x0

        # Select FPGAs to program
        self.smap_deselect_fpga([0, 1])
        self[self._global_register] = 0x3

        # Erase FPGAs SRAM
        self.erase_fpga(force=True)

        # Read bitstream
        with open(bitfile, "rb") as fp:
            data = fp.read()

        self.smap_select_fpga([0, 1])
        self[self._global_register] = 0x2

        fifo_register = 0x50001000

        start = time.perf_counter()
        # Check if ucp_smap_write is supported, otherwise fallback to UCP writes
        if cpld_version >= tpm_hw_definitions.CPLD_FW_VER_REQUIRED_FOR_SMAP_DOWNLOAD:
            logging.info("FPGA programming using fast SelectMap write")
            self._protocol.select_map_program(data, fifo_addr=None, ucp_smap_write=True)
        else:
            logging.info("FPGA programming using UCP write")
            self._protocol.select_map_program(data, fifo_addr=fifo_register, ucp_smap_write=False)

        # Wait for operation to complete
        status_read = 0
        for n, xil_register in enumerate(self._xil_registers):
            while self[xil_register] & 0x2 != 0x2:  # DONE high
                time.sleep(0.01)
                status_read += 1
                if status_read == 100:
                    logging.error("FPGA" + str(n) + " DONE is not high. This means that either the FPGA is not \
programmed or the CPLD is not detecting the DONE signal correctly due to an hardware issue. \
In the first case further errors will be detected later. It is necessary to power cycle the board, \
re-try configuration and check if this error disappears. In the second case initialisation might \
complete without errors and this message can be ignored, however this still highlights that \
a minor hardware issue affects the TPM.")
                    break

        end = time.perf_counter()
        logging.info("FPGA programming time: " + str(end - start) + "s")

        self.smap_deselect_fpga([0, 1])
        self[self._global_register] = 0x3

        # Brute force check to make sure we can communicate with programmed TPM
        for n in range(4):
            try:
                self.calibrate_fpga_to_cpld()
                magic0 = self[tpm_hw_definitions.FPGA_MAGIC_FPGA0_OFFSET]
                magic1 = self[tpm_hw_definitions.FPGA_MAGIC_FPGA1_OFFSET]
                if magic0 == magic1 == tpm_hw_definitions.FPGA_MAGIC:
                    return
                else:
                    logging.info("FPGA magic numbers are not correct %s, %s" % (hex(magic0), hex(magic1)))
            except:
                pass

            logging.info("Not possible to communicate with the FPGAs. Resetting CPLD...")
            self.write_address(0x30000008,0x8000,retry=False)  # Global Reset CPLD
            time.sleep(0.2)
            self.write_address(0x30000008,0x8000,retry=False)  # Global Reset CPLD
            time.sleep(0.2)

    def load_firmware(self, device, register_string=None, load_values=False, base_address=0):
        """ Override uperclass load_firmware to extract memory map from the bitfile
            :param base_address: base address at which to load firmware
            :param device: Device on board to load firmware to
            :param register_string: String containing register information
            :param load_values: Load register values
            """

        # Check if device is valid
        if device not in [Device.Board, Device.FPGA_1, Device.FPGA_2]:
            raise LibraryError("TPM devices can only be Board, FPGA_1 and FPGA_2")

        # Check if connected
        if not self._connected:
            raise LibraryError("Not connected to board, cannot load firmware")

        # Get FPGA base address and check if firmware is loaded in FPGA
        base_address = self['board.info.fpga1_base_add'] if device == Device.FPGA_1 \
            else self['board.info.fpga2_base_add']
        try:
            loaded = self['board.regfile.fpga1_programmed_fw'] if device == Device.FPGA_1 \
                else self['board.regfile.fpga2_programmed_fw']
        except:
            loaded = 1

        register = 'board.info.fw%d_xml_offset' % loaded
        if not self.memory_map.has_register(register):
            raise LibraryError("CPLD register information must be loaded prior to loading firmware")

        # Get XML file offset and read XML file from board
        register_string = self._get_xml_file(self[register])
        register_string = register_string.replace(b'<?xml version="1.0" ?>', b'')
        register_string = register_string.replace(b'id="tpm_test"',
                                                  b'id="fpga1"' if device == Device.FPGA_1 else b'id="fpga2"')
        register_string = "<node>\n{}\n</node>".format(register_string)

        # Call superclass with this file
        super(TPM, self).load_firmware(device=device, register_string=register_string,
                                       base_address=base_address, load_values=load_values)

    def is_programmed(self, fpga_id=None):
        """ Returns True if Board is programmed """

        if not self.is_connected():
            return None

        if not self._xml_loaded:
            return False

        if fpga_id is None:
            fpga_id_list = [0, 1]
        else:
            fpga_id_list = [fpga_id]

        fpga_programmed = [0, 0]
        for id in fpga_id_list:
            try:
                if id == 0:
                    magic = self[tpm_hw_definitions.FPGA_MAGIC_FPGA0_OFFSET]
                else:
                    magic = self[tpm_hw_definitions.FPGA_MAGIC_FPGA1_OFFSET]
                if magic == tpm_hw_definitions.FPGA_MAGIC:
                    fpga_programmed[id] = 1
            except:
                pass

        if fpga_id is not None:
            if fpga_programmed[fpga_id] == 1:
                return True
            else:
                return False

        self._programmed[Device.Board] = True
        self._programmed[Device.FPGA_1] = fpga_programmed[0]
        self._programmed[Device.FPGA_2] = fpga_programmed[1]

        return all(self._programmed.values())

    def set_lmc_ip(self, ip="10.0.10.1", port=None, port2=None, port3=None, same_port=True):
        """ Set the IP address for LMC data transfer
        :param ip: IP address in string form
        :param port: Port
        :param port2: Port for stream 1
        :param port3: Port for stream 2
        :param same_port: Use the same port for all ports
        """

        # Set Stream 0:
        # Set LMC IP
        self[0x30000038] = struct.unpack("!I", socket.inet_aton(ip))[0]

        # Set LMC destination and source port
        if port is None or type(port) is not int:
            port = self[0x30000044] & 0xFFFF

        self[0x30000044] = 10001 << 16 | port

        # Set Stream 1:
        # Set LMC IP
        self[0x30000048] = struct.unpack("!I", socket.inet_aton(ip))[0]

        # Set LMC destination and source port
        if port2 is not None and type(port) is int:
            self[0x3000004C] = 10001 << 16 | port2
        elif same_port:
            self[0x3000004C] = 10001 << 16 | port
        else:
            self[0x3000004C] = 10001 << 16 | self[0x3000004C] & 0xFFFF

        # Set Stream 2:
        # Set LMC IP
        self[0x30000050] = struct.unpack("!I", socket.inet_aton(ip))[0]

        # Set LMC destination and source port
        if port3 is not None and type(port) is int:
            self[0x30000054] = 10001 << 16 | port3
        elif same_port:
            self[0x30000054] = 10001 << 16 | port
        else:
            self[0x30000054] = 10001 << 16 | self[0x3000004C] & 0xFFFF

    def temperature(self):
        """ Get board temperature """
        if self[0x30000000] >= 0x19121600:
            temp = self[0x40000040]
        else:
            self[0x40000004] = 0x5
            self[0x40000000] = 0x2118
            time.sleep(0.02)
            temp = self[0x40000008]
            temp = ((temp >> 8) & 0x00FF) | ((temp << 8) & 0x1F00)
        return temp * 0.0625

    def voltage(self):
        """ Get board voltage """
        return self.tpm_lasc.get_voltage_5v0()

    def set_shutdown_temperature(self, temp):
        if self[0x30000000] >= tpm_hw_definitions.CPLD_FW_VER_REQUIRED_FOR_SHADOW_TEMPERATURE_TPM_V1_2:
            self[0x40000044] = int(temp / 0.0625)
            logging.info("Setting shutdown temperature to " + str(temp) + " degrees Celsius.")
        else:
            logging.info("Shutdown temperature not supported in CPLD firmware version older than 0x19121600")

    # ----------------------------------------- Private functions -----------------------------------------

    def _initialise_board(self):
        """ Initialise the TPM board """

        # Load SPI devices if XML file is available
        if self.memory_map.has_register('board.info.spi_xml_offset'):
            spi_xml = self._get_xml_file(self.read_register("board.info.spi_xml_offset"))
            self.load_spi_devices(spi_xml)

        # CPLD and SPI XML files have been loaded, check whether FPGA have been programmed
        # If FPGA is programmed, load the firmware's XML file
        if self.is_programmed(fpga_id=0):
            self.load_firmware(device=Device.FPGA_1)

        if self.is_programmed(fpga_id=1):
            self.load_firmware(device=Device.FPGA_2)

        return True

    def _initialise_devices(self,
                            fsample=800e6,
                            ddc=False,
                            fddc=0.0,
                            adc_clock_divider=1,
                            mono_channel_14_bit=False,
                            mono_channel_sel=0,
                            adc_low_bitrate=False,
                            disabled_adc=None):
        """ Initialise the SPI and other devices on the board """

        self.set_shutdown_temperature(68)

        # Switch voltages ON
        self['board.regfile.ctrl.en_ddr_vdd'] = 1

        # Reset and disable ADCs before enabling clock to avoid excessive power consumption when some ADCs are disabled.
        # There is a single wire from CPLD to all ADCs, so registers 0x3F must be set after soft reset and before
        # applying clock to the ADCs. Also reset the PLL here before starting.

        self['board.regfile.ctrl.ad9528_rst'] = 0

        available_adc = range(len(self.tpm_adc))
        enabled_adc = []
        for _ in available_adc:
            if _ in disabled_adc:
                pass
            else:
                enabled_adc.append(_)

        self['board.regfile.ctrl.ad_pdwn'] = 0x0
        for n in available_adc:
            self.tpm_adc[n].adc_reset()
            self.tpm_adc[n].adc_power_down_enable()
        for n in disabled_adc:
            logging.info("Disabling ADC " + str(n))
        self['board.regfile.ctrl.ad_pdwn'] = 0x1

        # Initialise PLL (3 retries)
        for i in range(3):
            try:
                self.tpm_pll.pll_start(fsample * adc_clock_divider)
                break
            except PluginError as err:
                if i == 2:
                    raise err

        # Initialise ADAs if required
        self.tpm_ada.disable_adas()
        if self._enable_ada:
            logging.info("Enabling ADAs")
            self.tpm_ada.initialise_adas()
            self.adas_enabled = True
        else:
            logging.info("Not enabling ADAs")

        # Initialise ADCs
        if self._enable_adc:
            if not ddc:
                if mono_channel_14_bit:
                    logging.info("Enabling single ADC channel with 14 bit sampling")
                for i in enabled_adc:
                    self.tpm_adc[i].adc_single_start(mono_channel_14_bit=mono_channel_14_bit,
                                                     clock_divider=adc_clock_divider,
                                                     low_bitrate=adc_low_bitrate,
                                                     mono_channel_sel=mono_channel_sel)
            else:
                logging.info("Enabling DDC, NCO frequency: %f Hz", fddc)
                for i in enabled_adc:
                    self.tpm_adc[i].adc_single_start_dual_14_ddc(sampling_frequency=fsample,
                                                                 ddc_frequency=fddc,
                                                                 low_bitrate=adc_low_bitrate)
            # else:
            #     logging.info("Enabling ADC clock divider %d" % adc_clock_divider)
            #     [self.tpm_adc[i].adc_single_start_dual_14_divider(adc_clock_divider, adc_low_bitrate) for i in
            #      range(len(self.tpm_adc))]

    def _get_xml_file(self, xml_offset):
        """ Get XML file from board
        :param xml_offset: Memory offset where XML file is stored
        :return: XML file as string
        """
        # Read length of XML file (first 4 bytes from offset)
        xml_len = self.read_address(xml_offset)

        # Read XML file from board
        zipped_xml = self.read_address(xml_offset + 4, int(ceil(xml_len / 4.0)))

        # Convert to string, decompress and return
        zipped_xml = ''.join([format(n, '08x') for n in zipped_xml])
        return zlib.decompress(binascii.unhexlify(zipped_xml[: 2 * xml_len]))

    # -------------------------- Methods for syntax candy --------------------------

    def __getitem__(self, key):
        """ Override __getitem__, return value from board """

        # Check if the specified key is a memory address or register name
        if isinstance(key, int):
            return self.read_address(key)

        # Check if the specified key is a tuple, in which case we are reading from a device
        if type(key) is tuple:
            # Run checks
            if not self._checks():
                return

            if len(key) == 2:
                return self.read_device(key[0], key[1])
            else:
                raise LibraryError("A device name and address need to be specified for writing to SPI devices")

        elif type(key) is str or isinstance(key, basestring):
            # Run checks
            if not self._checks():
                return

            # Check if a device is specified in the register name
            if self.memory_map.has_register(key):
                reg = self.memory_map[key]
                return self.read_register(key, reg.size)
        else:
            raise LibraryError("Unrecognised key type, must be register name, memory address or SPI device tuple")

        # Register not found
        raise LibraryError("Register %s not found" % key)

    def __setitem__(self, key, value):
        """ Override __setitem__, set value on board"""

        # Check is the specified key is a memory address or register name
        if isinstance(key, int):
            return self.write_address(key, value)

        # Check if the specified key is a tuple, in which case we are writing to a device
        if type(key) is tuple:
            # Run checks
            if not self._checks():
                return

            if len(key) == 2:
                return self.write_device(key[0], key[1], value)
            else:
                raise LibraryError("A device name and address need to be specified for writing to SPI devices")

        elif type(key) is str or isinstance(key, basestring):
            # Run checks
            if not self._checks():
                return

            # Check if device is specified in the register name
            if self.memory_map.has_register(key):
                return self.write_register(key, value)
        else:
            raise LibraryError(
            	"Unrecognised key type (%s), must be register name or memory address" % key.__class__.__name__)

        # Register not found
        raise LibraryError("Register %s not found" % key)

    def __str__(self):
        """ Override __str__ to print register information in a human readable format """

        if not self._programmed:
            return ""

        # Run checks
        if not self._checks():
            return

        # Split register list into devices
        registers = {}
        for k, v in self.memory_map.register_list.items():
            if v['device'] not in list(registers.keys()):
                registers[v['device']] = []
            registers[v['device']].append(v)

        # Loop over all devices
        string = "Device%sRegister%sAddress%sBitmask\n" % (' ' * 2, ' ' * 37, ' ' * 8)
        string += "------%s--------%s-------%s-------\n" % (' ' * 2, ' ' * 37, ' ' * 8)

        for k, v in registers.items():
            for reg in sorted(v, key=lambda arg: arg['name']):
                regspace = ' ' * (45 - len(reg['name']))
                adspace = ' ' * (15 - len(hex(reg['address'])))
                string += '%s\t%s%s%s%s0x%08X\n' % (
                    DeviceNames[k], reg['name'], regspace, hex(reg['address']), adspace, reg['bitmask'])

        # Add SPI devices
        if len(self._spi_devices) > 0:
            string += "\nSPI Devices\n"
            string += "-----------\n"

        for key in sorted(self._spi_devices.keys()):
            info = self._spi_devices[key]
            string += 'Name: %s\tspi_sclk: %d\tspi_en: %d\n' % (info, info.spi_sclk, info.spi_en)

        # Return string representation
        return string


# ------------------------------------------------ Tile Class ---------------------------------------

if __name__ == "__main__":
    tpm = TPM()
    tpm.connect(ip="10.0.10.2", port=10000)
