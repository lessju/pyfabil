from __future__ import division

import logging
from builtins import str
from pathlib import Path
from time import sleep

import pyfabil.boards.tpm_hw_definitions as tpm_hw_definitions
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from pyfabil.boards.tpm import TPM
from pyfabil.protocols.ucp import UCP


class TPM_1_6(TPM):
    """ Support for TPM v 1.6 """

    def __init__(self, **kwargs):
        """ Class constructor """
        kwargs['fpgaBoard'] = BoardMake.Tpm16Board
        kwargs['protocol'] = UCP

        # Set hardcoded CPLD xml offset address
        self._cpld_xml_offset = tpm_hw_definitions.CPLD_XML_OFFSET
        self._cpld_magic = tpm_hw_definitions.CPLD_MAGIC_TPM_V1_5

        # Initialise variables
        self._simulator = False
        self._initialise = False
        self._enable_ada = False
        self.adas_enabled = False
        self._enable_adc = True

        # CPLD SelectMap Registers
        self._xil_registers = ['board.smap.xil_0', 'board.smap.xil_1']
        self._global_register = 'board.smap.global'
        self._c2c_stream_enable = 'board.regfile.ena_stream'
        self.download_firmware_required_cpld_version = 0

        self.tpm_hw_rev_1_5 = tpm_hw_definitions.TPM_HW_REV_1_5
        self.tpm_hw_rev_2_0 = tpm_hw_definitions.TPM_HW_REV_2_0
        self.board_info = None
        self.hw_rev = None

        self.CPLD_FW_VER_LOCK_I2C_CHANGE = tpm_hw_definitions.CPLD_FW_VER_LOCK_I2C_CHANGE_TPM_V1_5
        self.MCU_FW_VER_LOCK_I2C_CHANGE = tpm_hw_definitions.MCU_FW_VER_LOCK_I2C_CHANGE_TPM_V1_5
        self.MCU_FW_VER_ERROR_NO_MCU = tpm_hw_definitions.MCU_FW_VER_ERROR_NO_MCU

        # Call superclass initialiser
        # Call superclass initialiser
        super(TPM, self).__init__(**kwargs)

        # Try importing itpm_bios_v1_6.bios, if not available use default BIOS version definitions
        try:
            import itpm_bios_v1_6.bios
            self.BIOS_REV_list = itpm_bios_v1_6.bios.bios_get_dict('itpm_v1.6')
        except ModuleNotFoundError:
            self.BIOS_REV_list = [
                ["0.0.1", "CPLD_0x19121901-MCU_0xb0000010_0x0_0x0"],
                ["0.0.2", "CPLD_0x19122316-MCU_0xb0000010_0x0_0x0"],
                ["0.0.3", "CPLD_0x20011611-MCU_0xb0000010_0x0_0x0"],
                ["0.0.4", "CPLD_0x20011611-MCU_0xb0000016_0x20200123_0x113426"],
                ["0.0.5", "CPLD_0x20041613-MCU_0xb0000016_0x20200123_0x113426"],
                ["0.1.0", "CPLD_0x20111309-MCU_0xb0000100_0x20201120_0xe3f6fa2"],
                ["0.2.0", "CPLD_0x21033009-MCU_0xb0000107_0x20210114_0x0"],
                ["0.2.1", "CPLD_0x21061417-MCU_0xb0000117_0x20210615_0x0"],
                ["0.2.2", "CPLD_0x22032409-MCU_0xb0000118_0x20220831_0x0"],
                ["0.3.0", "CPLD_0x23011618-MCU_0xb000011a_0x20230126_0x0"]
            ]

        self.BOARD_MODE = {
            "ada":    0,
            "no-ada": 0xff
        }

    def connect(self, ip, port,
                fsample=800e6,
                ddc=False,
                fddc=0.0,
                adc_clock_divider=1,
                mono_channel_14_bit=False,
                mono_channel_sel=0,
                adc_low_bitrate=False,
                **kwargs):
        """ Overload connect method
        :param fsample: Sampling rate
        :param ip: IP address to connect to
        :param port: Port to connect to
        """

        # Check if we are simulating or not
        self._simulator = kwargs.get('simulator', False)

        # Check if we are initialising or not
        self._initialise = kwargs.get('initialise', False)

        # TPM might or might not have an ADA
        self._enable_ada = kwargs.get("enable_ada", False)

        # You can skip ADC enabling for test/debug purpose (default True to preserve back compatibility)
        self._enable_adc = kwargs.get("enable_adc", True)

        # Call connect on super class
        super(TPM, self).connect(ip, port, **kwargs)

        # CPLD is assumed programmed
        self._programmed[Device.Board] = True

        # Try to read address 0x30000000 (CPLD version) check whether TPM is reachable
        try:
            self.read_address(tpm_hw_definitions.CPLD_VERSION_OFFSET)
        except:
            self.status[Device.Board] = Status.NetworkError
            self._connected = False
            raise LibraryError("Could not reach TPM with address {}".format(ip))

        _tmp = self.read_address(tpm_hw_definitions.CPLD_MAGIC_OFFSET)
        if _tmp != self._cpld_magic:
            raise LibraryError("TPM CPLD_MAGIC mismatch. Expected 0x%x, got 0x%x." % (self._cpld_magic, _tmp))

        # Get XML file
        cpld_xml = str(self._get_xml_file(self.read_address(self._cpld_xml_offset)))
        cpld_xml = cpld_xml.replace('<?xml version="1.0" ?>', '')
        cpld_xml = "<node>\n{}\n</node>".format(cpld_xml)

        super(TPM, self).load_firmware(device=Device.Board, register_string=cpld_xml)
        logging.info("tpm.connect: CPLD XML File Downloaded")

        # Load plugins
        self.load_plugins()

        # Initialise devices if required
        if not self._simulator and self._connected:
            if self._initialise:
                self._initialise_devices(fsample=fsample,
                                         ddc=ddc, fddc=fddc,
                                         adc_clock_divider=adc_clock_divider,
                                         mono_channel_14_bit=mono_channel_14_bit,
                                         mono_channel_sel=mono_channel_sel,
                                         adc_low_bitrate=adc_low_bitrate)
            # else:
            #    self.tpm_pll.pll_reset()

    def load_plugins(self):
        """ Overloaded load_plugins function to load plugins
            for TPM 1.6 """

        # Load the CPLD plugin (does not require the board to be initialised)
        self.load_plugin("Tpm_1_6_ProgFlash")
        self.load_plugin("Tpm_1_6_Cpld")
        self.load_plugin("Tpm_1_6_EEP")
        self.load_plugin("Tpm_1_6_Mcu")
        self.i2c_old_mode = False
        if self[tpm_hw_definitions.CPLD_MCU_VERSION_OFFSET] != self.MCU_FW_VER_ERROR_NO_MCU:
            if self["board.regfile.date_code"] <= self.CPLD_FW_VER_LOCK_I2C_CHANGE \
                or self[tpm_hw_definitions.CPLD_MCU_VERSION_OFFSET] <= self.MCU_FW_VER_LOCK_I2C_CHANGE:
                self.i2c_old_mode = True
        else:
            logging.warning("Board is running without MCU, reduced functionality available!")
            self.i2c_old_mode = True
        # Read Hardware information from EEPROM
        self.board_info = self.get_board_info()
        self.hw_rev = self.get_hardware_revision()
        logging.info("Board SN: %s" % self.board_info["SN"])
        logging.info("Board HARDWARE_REV: %s" % self.board_info["HARDWARE_REV"])
        logging.info("Board bios: %s" % self.board_info["bios"])
        if self['board.regfile.key_ok'] == 0:
            logging.warning("Board is running without License, functionality will be corrupted after 1 hour from power-up")
            if self['board.regfile.timebomb_status'] == 0:
                logging.error("License timebomb already expired! Please power off and power on the board to recover functionality")
                raise LibraryError("License timebomb already expired! Please power off and power on the board to recover functionality")

        if not self._simulator and self._connected:

            # Load QSFP_ADAPTER plugins
            self.load_plugin("Tpm_1_6_QSFPAdapter", core_id=0)
            self.load_plugin("Tpm_1_6_QSFPAdapter", core_id=1)

            # Load PREADU plugins
            self.load_plugin("Tpm_1_6_PreAdu", preadu_id=0)
            self.load_plugin("Tpm_1_6_PreAdu", preadu_id=1)

            # Try initialising the board (will check if FPGAs are programmed)
            if not self._initialise_board():
                return

            # Pre-load all required plugins. Board-level devices are loaded here
            [self.load_plugin("TpmFirmwareInformation", firmware=x) for x in range(1, 4)]

            # Load ADAs
            # ADA not implemented in TPM 1.6

            # Load ADCs
            [self.load_plugin("TpmAdc9695", adc_id=adc) for adc in
                ["adc0", "adc1", "adc2", "adc3", "adc4", "adc5", "adc6", "adc7",
                 "adc8", "adc9", "adc10", "adc11", "adc12", "adc13", "adc14", "adc15"]]

            # Load PLL
            self.load_plugin("Tpm_1_6_Pll")

            # Update firmware information
            if self.is_programmed():
                [info.update_information() for info in self.tpm_firmware_information]

    def erase_fpga(self,force=True):
        """ Erase FPGAs configuration memory """

        def smap_data_format(dat):
            def reverse_bit(num):
                result = 0
                for n in range(8):
                    result = (result << 1) + (num & 1)
                    num >>= 1
                return result

            smap_dat = 0
            for n in range(4):
                smap_dat |= reverse_bit(dat >> 8 * n) << 8 * n
            return smap_dat

        # Check if connected
        if not self.is_connected():
            raise LibraryError("TPM needs to be connected in order to program FPGAs")

        self[self._c2c_stream_enable] = 0x0  # Disable C2C stream

        # Temporary
        self[self._global_register] = 0x3

        # Erase FPGAs SRAM
        for fpga_id in range(len(self._xil_registers)):
            xil_register = self._xil_registers[fpga_id]

            logging.info("Erasing FPGA%d using PROG pin method" % fpga_id)
            self[xil_register] = 0x10  # Select FPGA
            self[self._global_register] = 0x0  # PROG = 0

            max_attempt = 10
            attempt = 0
            fpga_still_programmed = 0
            while self[xil_register] & 0x1 == 1:  # wait for INIT
                if attempt == max_attempt:
                    fpga_still_programmed = 1
                    break
                attempt += 1
                sleep(0.1)
            self[self._global_register] = 0x3
            while self[xil_register] & 0x1 == 0:  # wait for INIT
                if attempt == max_attempt:
                    fpga_still_programmed = 1
                    break
                attempt += 1
                sleep(0.1)

            # If FPGA erase with PROG pin method timed out, try with ICAP3 method
            if fpga_still_programmed == 1:
                logging.error("Erasing FPGA%d using PROG pin method failed" % fpga_id)
                raise LibraryError("Erasing FPGA%d using PROG pin method failed" % fpga_id)
        self[self._global_register] = 0x3

    def download_firmware(self, device, bitfile):
        """ Download bitfile to FPGA
        :param device: FPGA to download bitfile
        :param bitfile: Bitfile to download
        """
        if self["board.regfile.enable.fpga"] == 0:
            logging.info("FPGAs power disabled. Enabling")
            self["board.regfile.enable.fpga"] = 1
            time.sleep(0.5)
        else:
            logging.info("FPGAs power already enabled.")

        # Lock SMAP interface to avoi MCU access conflict
        start_lock = time.time()
        if self["board.regfile.date_code"] <= self.CPLD_FW_VER_LOCK_I2C_CHANGE:
            logging.info("Locking SMAP interface to avoid MCU access conflict")
            locked = False
            retry = 10
            while retry > 0:
                self["board.lock.mlock1"] = 0x50000000
                if self["board.lock.mlock1"] != 0x50000000:
                    retry = retry-1
                    time.sleep(0.01)
                else:
                    locked = True
                    break
        else:
            logging.info("Locking SMAP interface to avoid MCU access conflict")
            locked = False
            retry = 10
            ticket_num = self["board.lock.queue_number"]
            while retry > 0:
                self["board.lock.lock_smap"] = ticket_num
                if self["board.lock.lock_smap"] != ticket_num:
                    retry = retry-1
                    time.sleep(0.01)
                else:
                    locked = True
                    break
        if locked is False:
            logging.error("ERROR: Can't Lock SMAP interface")
            raise LibraryError("ERROR: Can't Lock SMAP interface")
        # Program FPGA
        # super(TPM_1_6, self).download_firmware(device, bitfile)

        """ Download bitfile to FPGA
        :param device: FPGA to download bitfile
        :param bitfile: Bitfile to download
        """
        # Check if connected
        if self.status[Device.Board] != Status.OK:
            raise LibraryError("TPM needs to be connected in order to program FPGAs")

        required_cpld_version = self.download_firmware_required_cpld_version
        cpld_version = self[tpm_hw_definitions.CPLD_VERSION_OFFSET]
        if cpld_version < required_cpld_version or cpld_version & 0xF0 == 0xB0:
            logging.error("CPLD firmware version is too old. Required version is " + hex(required_cpld_version))
            raise LibraryError(
                "CPLD firmware version is too old. Required version is " + hex(required_cpld_version))

        # rearm lock
        if self["board.regfile.date_code"] > self.CPLD_FW_VER_LOCK_I2C_CHANGE:
            self["board.lock.lock_smap"] = ticket_num
        # Sanity checks on provided bitfile (exists and size)
        f = Path(bitfile)
        if not f.exists():
            raise LibraryError("Provided bitfile ({}) does not exist".format(bitfile))
        elif f.stat().st_size < 1024:
            raise LibraryError(
                "Provided bitfile ({}) is too small ({} bytes)".format(bitfile, f.stat().st_size))

        # Disable C2C stream
        self[self._c2c_stream_enable] = 0x0

        # Select FPGAs to program
        self.smap_deselect_fpga([0, 1])
        self[self._global_register] = 0x3

        # Erase FPGAs SRAM
        self.erase_fpga(force=True)

        # Read bitstream
        with open(bitfile, "rb") as fp:
            data = fp.read()

        self.smap_select_fpga([0, 1])
        self[self._global_register] = 0x2

        fifo_register = 0x50001000

        # rearm lock
        if self["board.regfile.date_code"] > self.CPLD_FW_VER_LOCK_I2C_CHANGE:
            self["board.lock.lock_smap"] = ticket_num

        start = time.perf_counter()
        # Check if ucp_smap_write is supported, otherwise fallback to UCP writes
        # if ucp_smap_write is not supported, use normal UCP access
        if cpld_version < tpm_hw_definitions.CPLD_FW_VER_REQUIRED_FOR_SMAP_DOWNLOAD:
            logging.info("FPGA programming using UCP write")
            self._protocol.select_map_program(data, fifo_addr=fifo_register, ucp_smap_write=False)
        else:
            if cpld_version <= self.CPLD_FW_VER_LOCK_I2C_CHANGE:
                logging.info("FPGA programming using fast SelectMap write")
                self._protocol.select_map_program(data, fifo_addr=None, ucp_smap_write=True)
            else:
                logging.info("FPGA programming using fast SelectMap with SMAP lock")
                self._protocol.select_map_program(data, fifo_addr=None, ucp_smap_write=True, lock_address=0x6000000c,
                                                  lock_value=ticket_num)

        # Wait for operation to complete
        status_read = 0
        for n, xil_register in enumerate(self._xil_registers):
            while self[xil_register] & 0x2 != 0x2:  # DONE high
                time.sleep(0.01)
                status_read += 1
                if status_read == 100:
                    logging.error("FPGA" + str(n) + " DONE is not high. This means that either the FPGA is not \
programmed or the CPLD is not detecting the DONE signal correctly due to an hardware issue. \
In the first case further errors will be detected later. It is necessary to power cycle the board, \
re-try configuration and check if this error disappears. In the second case initialisation might \
complete without errors and this message can be ignored, however this still highlights that \
a minor hardware issue affects the TPM.")
                    break

        end = time.perf_counter()
        logging.info("FPGA programming time: " + str(end - start) + "s")

        self.smap_deselect_fpga([0, 1])
        self[self._global_register] = 0x3

        # Brute force check to make sure we can communicate with programmed TPM
        magic_ok = False
        for n in range(4):
            try:
                self.calibrate_fpga_to_cpld()
                magic0 = self[tpm_hw_definitions.FPGA_MAGIC_FPGA0_OFFSET]
                magic1 = self[tpm_hw_definitions.FPGA_MAGIC_FPGA1_OFFSET]
                if magic0 == magic1 == tpm_hw_definitions.FPGA_MAGIC:
                    magic_ok = True
                    break
                else:
                    logging.info("FPGA magic numbers are not correct %s, %s" % (hex(magic0), hex(magic1)))
            except:
                pass

            # !TODO: fix CPLD reset to work with TPM 1.6
            # logging.info("Not possible to communicate with the FPGAs. Resetting CPLD...")
            # self.write_address(0x30000008, 0x8000, retry=False)  # Global Reset CPLD
            # time.sleep(0.2)
            # self.write_address(0x30000008, 0x8000, retry=False)  # Global Reset CPLD
            time.sleep(0.2)

        # Unlock SMAP interface
        end_lock = time.time()
        logging.info("Unlocking SMAP interface: elapsed time %s", str(end_lock - start_lock))
        if self["board.regfile.date_code"] <= self.CPLD_FW_VER_LOCK_I2C_CHANGE:
            self["board.lock.mlock1"] = 0xFFFFFFFF
        else:
            self["board.lock.lock_smap"] = 0
        if magic_ok:
            return
        else:
            raise LibraryError("Not possible to communicate with the FPGAs.")

    def is_programmed(self, fpga_id=None):
        """ Returns True if Board is programmed """

        if not self.is_connected():
            logging.debug("tpm.is_programmed")
            return None

        logging.debug("FPGAs is_programmed? Enable %d, Done[0-1] %d" % (
        self['board.regfile.enable.fpga'], self['board.regfile.xilinx.done']))
        self._programmed[Device.FPGA_1] = self['board.regfile.enable.fpga'] and bool(
            self['board.regfile.xilinx.done'] & 0x1)
        self._programmed[Device.FPGA_2] = self['board.regfile.enable.fpga'] and bool(
            self['board.regfile.xilinx.done'] & 0x2)

        if fpga_id is None:
            if (not self._programmed[Device.FPGA_1]) and (not self._programmed[Device.FPGA_2]):
                logging.debug("Return False")
                return False
            logging.debug("Return True")
            return True
        else:
            if fpga_id == 0:
                logging.debug("Return %s" % self._programmed[Device.FPGA_1])
                return bool(self._programmed[Device.FPGA_1])
            elif fpga_id == 1:
                logging.debug("Return %s" % self._programmed[Device.FPGA_2])
                return bool(self._programmed[Device.FPGA_2])
        return None

    def set_lmc_ip(self, ip="10.0.10.1", port=None, port2=None, port3=None, same_port=True):
        """ Set the IP address for LMC data transfer
        :param ip: IP address in string form
        :param port: Port
        :param port2: Port for stream 1
        :param port3: Port for stream 2
        :param same_port: Use the same port for all ports
        """
        # Set Stream 0:
        # Set LMC IP
        self['board.regfile.stream_dst_ip0'] = struct.unpack("!I", socket.inet_aton(ip))[0]

        # Set LMC destination and source port
        if port is None or type(port) is not int:
            port = self['board.regfile.stream_dst_port0'] & 0xFFFF

        self['board.regfile.stream_src_port0'] = 10001
        self['board.regfile.stream_dst_port0'] = port

        # Set Stream 1:
        # Set LMC IP
        self['board.regfile.stream_dst_ip1'] = struct.unpack("!I", socket.inet_aton(ip))[0]

        # Set LMC destination and source port
        self['board.regfile.stream_src_port1'] = 10001
        if port2 is not None and type(port) is int:
            self['board.regfile.stream_dst_port1'] = port2
        elif same_port:
            self['board.regfile.stream_dst_port1'] = port

        # # Set Stream 2:
        # # Set LMC IP
        # self['board.regfile.stream_dst_ip2']  = struct.unpack("!I", socket.inet_aton(ip))[0]
        #
        # # Set LMC destination and source port
        # self['board.regfile.stream_src_port2'] = 10001
        # if port3 is not None and type(port) is int:
        #     self['board.regfile.stream_dst_port2'] = port3
        # elif same_port:
        #     self['board.regfile.stream_dst_port2'] = port

    def temperature(self):
        """ Get board temperature """
        return self.tpm_monitor.get_temperature()

    def voltage(self):
        """ Get board voltage """
        return self.tpm_monitor.get_voltage_5v0()

    def set_shutdown_temperature(self, temp):
        logging.info("Shutdown temperature not supported in iTPM 1.6 use set_board_alm_temp_thresholds in tpm_monitor")

    def get_part_number(self):
        return self.tpm_eep.get_field("PN")

    def get_serial_number(self):
        return self.tpm_eep.get_field("SN")

    def get_hardware_revision(self):
        hw_rev_arr = self.tpm_eep.get_field("HARDWARE_REV")
        hw_rev = 0
        for byte in hw_rev_arr:
            hw_rev = hw_rev * 256 + byte
        if hw_rev == 0xffffff or hw_rev == 0x0:
            logging.error("Could not read HARDWARE_REV from EEPROM, returned: " + hex(hw_rev))
        return hw_rev

    def get_bios(self):
        string = "CPLD_"
        string += self.tpm_cpld.get_version()
        string += "-MCU_"
        string += self.tpm_monitor.get_version()
        final_string = "v?.?.? (%s)" % string
        for BIOS_REV in self.BIOS_REV_list:
            if BIOS_REV[1] == string:
                final_string = "v%s (%s)" % (BIOS_REV[0], string)
                break
        return final_string

    def get_mac(self):
        mac = self.tpm_eep.get_field("MAC")
        mac_str = ""
        for i in range(0,len(mac)-1):
            mac_str += '{0:02x}'.format(mac[i])+":"
        mac_str += '{0:02x}'.format(mac[len(mac)-1])
        return mac_str

    def get_board_info(self):
        if self["board.regfile.date_code"] <= self.CPLD_FW_VER_LOCK_I2C_CHANGE:
            tpm_info = {"ip_address": long2ip(self['board.regfile.eth_ip']),
                        "netmask": long2ip(self['board.regfile.eth_mask']),
                        "gateway": long2ip(self['board.regfile.eth_gway']),
                        "ip_address_eep": self.tpm_eep.get_field("ip_address"),
                        "netmask_eep": self.tpm_eep.get_field("netmask"),
                        "gateway_eep": self.tpm_eep.get_field("gateway"),
                        "MAC": self.get_mac(),
                        "SN": self.get_serial_number(),
                        "PN": self.get_part_number(),
                        "bios": self.get_bios(),
                        }
        else:
            tpm_info = {"ip_address": long2ip(self['board.i2c.ip']),
                        "netmask": long2ip(self['board.i2c.netmask']),
                        "gateway": long2ip(self['board.i2c.gateway']),
                        "ip_address_eep": self.tpm_eep.get_field("ip_address"),
                        "netmask_eep": self.tpm_eep.get_field("netmask"),
                        "gateway_eep": self.tpm_eep.get_field("gateway"),
                        "MAC": self.get_mac(),
                        "SN": self.get_serial_number(),
                        "PN": self.get_part_number(),
                        "bios": self.get_bios(),
                        }

        if self.tpm_eep.get_field("BOARD_MODE") == self.BOARD_MODE['ada']:
            tpm_info["BOARD_MODE"] = "ADA"

        elif self.tpm_eep.get_field("BOARD_MODE") == self.BOARD_MODE['no-ada']:
            tpm_info["BOARD_MODE"] = "NO-ADA"
        else:
            tpm_info["BOARD_MODE"] = "UNKNOWN"

        location = [self.tpm_eep.get_field("CABINET_LOCATION"),
                    self.tpm_eep.get_field("SUBRACK_LOCATION"),
                    self.tpm_eep.get_field("SLOT_LOCATION")]
        tpm_info["LOCATION"] = str(location[0]) + ":" + str(location[1]) + ":" + str(location[2])

        pcb_rev = self.tpm_eep.get_field("PCB_REV")
        if pcb_rev == 0xff:
            pcb_rev_string = ""
        else:
            pcb_rev_string = str(pcb_rev)

        hw_rev = self.tpm_eep.get_field("HARDWARE_REV")
        tpm_info["HARDWARE_REV"] = "v" + str(hw_rev[0]) + "." + str(hw_rev[1]) + "." + str(hw_rev[2]) + pcb_rev_string

        ddr_size = self.tpm_eep.get_field("DDR_SIZE_GB")
        if ddr_size == 0xff:
            tpm_info["DDR_SIZE_GB"] = "4"
        else:
            tpm_info["DDR_SIZE_GB"] = str(ddr_size)
        return tpm_info

    def get_global_status_alarms(self):
        alarms = {"I2C_access_alm": (self['board.regfile.global_status'] & 0xf0000) >> 16,
                  "temperature_alm": self['board.regfile.global_status.temperature'],
                  "voltage_alm": self['board.regfile.global_status.voltage'],
                  "SEM_wd": self['board.regfile.global_status.SEM'],
                  "MCU_wd": self['board.regfile.global_status.MCU']}
        return alarms



    def _initialise_devices(self,
                            fsample=800e6,
                            ddc=False,
                            fddc=0.0,
                            adc_clock_divider=1,
                            mono_channel_14_bit=False,
                            mono_channel_sel=0,
                            adc_low_bitrate=False,
                            disabled_adc=[]):
        """ Initialise the SPI and other devices on the board """

        # self.set_shutdown_temperature(65)

        # Switch voltages ON
        # self['board.regfile.ctrl.en_ddr_vdd'] = 1

        available_adc = range(len(self.tpm_adc))
        enabled_adc = []
        for _ in available_adc:
            if _ in disabled_adc:
                pass
            else:
                enabled_adc.append(_)

        # TODO: this works on TPM 1.2 but not on TPM 1.6
        # self['board.regfile.ctrl.ad_pdwn'] = 0x0
        # for n in available_adc:
        #     self.tpm_adc[n].adc_reset()
        #     self.tpm_adc[n].adc_power_down_enable()
        # for n in disabled_adc:
        #     logging.info("Disabling ADC " + str(n))
        # self['board.regfile.ctrl.ad_pdwn'] = 0x1

        # Initialise PLL (3 retries)
        logging.info("Configuring AD9528 PLL at " + str(fsample) + " Hz")
        for i in range(3):
            try:
                self.tpm_pll.pll_start(fsample)
                break
            except PluginError as err:
                if i == 2:
                    raise err

        # Initialise ADAs if required
        # self.tpm_ada.disable_adas()
        # if self._enable_ada:
        #     logging.info("Enabling ADAs")
        #     self.tpm_ada.initialise_adas()
        #     self.adas_enabled = True
        # else:
        #     logging.info("Not enabling ADAs")

        # Initialise ADCs
        if self._enable_adc:
            if not ddc:
                if mono_channel_14_bit:
                    logging.info("Enabling single ADC channel with 14 bit sampling")
                for i in enabled_adc:
                    self.tpm_adc[i].adc_single_start(mono_channel_14_bit=mono_channel_14_bit,
                                                     mono_channel_sel=mono_channel_sel)
            else:
                logging.info("Enabling DDC, NCO frequency: %f Hz", fddc)
                for i in enabled_adc:
                    self.tpm_adc[i].adc_single_start_dual_14_ddc(sampling_frequency=fsample,
                                                                 ddc_frequency=fddc,
                                                                 decimation_factor=20,
                                                                 low_bitrate=adc_low_bitrate)



if __name__ == "__main__":

    # Enable debug logging
    from sys import stdout
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    line_format = logging.Formatter("%(asctime)s - %(levelname)s - %(threadName)s - %(message)s")
    ch = logging.StreamHandler(stdout)
    ch.setFormatter(line_format)
    log.addHandler(ch)

    # Test TPM
    tpm = TPM_1_6(ip="10.0.10.2")
    tpm.connect(ip="10.0.10.2", port=10000)
    print(tpm.get_board_info())
