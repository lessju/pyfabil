from builtins import object
import xml.etree.ElementTree as eTree

from pyfabil import LibraryError, SPIDeviceInfo


class SPI(object):
    """ A class which handles information on SPI devices """

    def __init__(self, xml_string):
        """ class constructor
         :param xml_string: XML string containing SPI device information """

        # SPI device list
        self.spi_map = {}

        # SPI selection
        self.spi_address = None
        self.spi_address_mask = None
        self.write_data = None
        self.write_data_mask = None
        self.read_data = None
        self.read_data_mask = None
        self.chip_select = None
        self.chip_select_mask = None
        self.sclk = None
        self.sclk_mask = None
        self.cmd_address = None
        self.cmd_start_mask = None
        self.cmd_rnw_mask = None

        # Process xml string
        self._process_xml_string(xml_string)

    def clear(self):
        """ Clear SPI map """
        self.spi_map = {}

    def _process_xml_string(self, xml_string):
        """ Process xml string containing SPI device information
        :param xml_string: XML string containing SPI device information"""

        # Parse string and get root
        tree = eTree.fromstring(xml_string)

        self.clear()

        # Iterate through all child node to get SPI devices
        for device_node in tree:
            # Get device information
            try:
                device_id = device_node.attrib['id']
                device = SPIDeviceInfo(name=device_id,
                                       spi_en=int(device_node.attrib['spi_en']),
                                       spi_sclk=int(device_node.attrib['spi_sclk']))

                self.spi_map[device_id] = device
            except:
                raise LibraryError("Missing information in SPI file")

    def has_device(self, device):
        """ Check if spi map contains specified device
        :param device: Device to check """
        return device in list(self.spi_map.keys())

    def __getitem__(self, key):
        """ Override __getitem__, return SPI device information for specified register """
        # If no spi map exists, return None
        if len(self.spi_map) == 0:
            raise LibraryError("Cannot get device from uninitialised SPI map")

        if key not in list(self.spi_map.keys()):
            raise LibraryError("Specified SPI device {}, does not exist in SPI map")

        # Register found, return information
        return self.spi_map[key]

    def __len__(self):
        return len(self.spi_map)
