from math import ceil
import re
import zlib
import binascii

__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
import logging


class TpmFirmwareInformation(FirmwareBlock):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.TpmBoard,BoardMake.Tpm16Board)
    @friendlyname('tpm_firmware_information')
    @maxinstances(3)
    def __init__(self, board, **kwargs):
        """ TpmPll initialiser
        :param board: Pointer to board instance
        """
        super(TpmFirmwareInformation, self).__init__(board)

        if 'firmware' not in list(kwargs.keys()):
            raise PluginError("TpmFirmwareInformation: Require a firmware number")
        self._firmware = kwargs['firmware']

        self._register = 'board.info.fw%d_extended_info_offset' % self._firmware

        # Initialise information
        self._reset_information()


    #######################################################################################

    def parse_extended_info(self, extended_info):
        r_dict = {}
        for item in extended_info.split('\n\n'):
            if item:
                key = item.split(': ', 1)[0].strip().lower()
                val = item.split(': ', 1)[1].strip().lower()
                r_dict[key] = val
        return r_dict

    def update_information(self):
        """ Update firmware extended information
        :return:
        """

        # Read information data from board
        offset = self.board[self._register]
        size = self.board[int(offset)]
        
        # Make sure that the size is not huge (random memory value)
        if size > 1e6 or size in [0, 1]:
            logging.error(f'Extended information size is {size}')
            self._reset_information()
            return

        data = self.board.read_address(offset + 4, n = int(ceil(size / 4.0)))
        data = ''.join([format(n, '08x') for n in data])
        try:
            data = zlib.decompress(binascii.unhexlify(data[: 2 * size]))
        except Exception as e:
            logging.warning(f'Error reading extended info for firmware {self._firmware} {e} ')
            self._reset_information()
            return

        res = self.parse_extended_info(data.decode())

        if res.get('design') is None:
            self._reset_information()
        else:
            self._major = int(res['major'])
            self._minor = int(res['minor'])
            self._host  = res['host']
            self._design = res['design']
            self._user = res['user']
            self._time = res['utc_compile_time']
            self._build = res['build']
            self._board = res['board']
            # Use .get for optional information, old firmware
            # will not have this information
            self._git_branch = res.get('git_branch', "")
            self._git_commit = res.get('git_commit', "")
            self._git_dirty_flag = res.get('git_dirty_flag', "")

    def _reset_information(self):
        """ Reset firmware information """
        self._major = -1
        self._minor = -1
        self._host  = ""
        self._design = ""
        self._user = ""
        self._time = ""
        self._build = ""
        self._board = ""
        self._git_branch = ""
        self._git_commit = ""
        self._git_dirty_flag = ""

    def print_information(self):
        """ Print firmware information """
        logging.info("Board: %s" % self._board)
        logging.info("Design: %s" % self._design)
        logging.info("Major version: %d" % self._major)
        logging.info("Minor version: %d" % self._minor)
        logging.info("Build: %s" % self._build)
        logging.info("UTC compile time: %s" % self._time)
        logging.info("User: %s" % self._user)
        logging.info("Git Branch: %s" % self._git_branch)
        logging.info("Git Commit: %s" % self._git_commit)
        logging.info("Git Dirty Flag: %s" % self._git_dirty_flag)
        logging.info("Host: %s" % self._host)

    # Information getters
    def get_firmware_number(self): return self._firmware
    def get_major_version(self):   return self._major
    def get_minor_version(self):   return self._minor
    def get_host(self):            return self._host
    def get_design(self):          return self._design
    def get_user(self):            return self._user
    def get_time(self):            return self._time
    def get_build(self):           return self._build
    def get_board(self):           return self._board

    ##################### Superclass method implementations #################################


    def initialise(self):
        """ Initialise TpmPll """
        logging.info("TpmFirmwareInformation has been initialised")
        return True


    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmFirmwareInformation : Checking status")
        return Status.OK


    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmFirmwareInformation : Cleaning up")
        return True
