from builtins import object

__author__ = 'chiello'

import pyfabil.boards.tpm_hw_definitions as tpm_hw_definitions
from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
import logging
import time

class TpmI2c(FirmwareBlock):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.TpmBoard)
    @friendlyname('tpm_i2c')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmI2c initialiser
        :param board: Pointer to board instance
        """
        super(TpmI2c, self).__init__(board)

        self._board_type = kwargs.get('board_type', 'XTPM')

        self.retries = 5
        self.polling_time = 0.01
        if self.board['board.regfile.date_code'] >= 0x23041900:
            self.command_done_supported = True
        else:
            self.command_done_supported = False

    #######################################################################################
    def write(self, i2c_add, nof_wr_byte, nof_rd_byte, wr_data):
        logging.debug("I2C write: accessing device " + hex(i2c_add))
        add = i2c_add >> 1
        cmd = (nof_rd_byte << 12) + (nof_wr_byte << 8) + add

        for _ in range(self.retries):
            if _ > 0:
                logging.warning("I2C write: retrying access")
            self.board['board.i2c.transmit'] = wr_data
            self.board['board.i2c.command'] = cmd

            # check if command has been executed
            if self.command_done_supported:
                for _p in range(10):
                    command_done = self.board['board.i2c.command'] & 0x40000
                    if command_done != 0:
                        break
                    else:
                        time.sleep(self.polling_time)

            # check results
            for _p in range(10):
                status = self.board['board.i2c.status'] & 0x3
                if status == 2:
                    logging.warning("I2C write: NACK received")
                    time.sleep(polling_time)
                elif status == 0:
                    return
                else:
                    time.sleep(self.polling_time)

        logging.error("I2C write failed.")

    def read(self, i2c_add, nof_wr_byte, nof_rd_byte, wr_data):
        logging.debug("I2C read: accessing device " + hex(i2c_add))
        add = i2c_add >> 1
        cmd = (nof_rd_byte << 12) + (nof_wr_byte << 8) + add

        for _ in range(self.retries):
            if _ > 0:
                logging.warning("I2C read: retrying access")
            self.board['board.i2c.transmit'] = wr_data
            self.board['board.i2c.command'] = cmd

            # check if command has been executed
            if self.command_done_supported:
                for _p in range(10):
                    command_done = self.board['board.i2c.command'] & 0x40000
                    if command_done != 0:
                        break
                    else:
                        time.sleep(self.polling_time)

            # check results
            for _p in range(10):
                status = self.board['board.i2c.status'] & 0x3
                if status == 2:
                    logging.warning("I2C read: NACK received")
                    time.sleep(polling_time)
                elif status == 0:
                    rdata = self.board['board.i2c.receive']
                    return rdata
                else:
                    time.sleep(self.polling_time)

        logging.error("I2C read failed.")

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmI2c """
        logging.info("TpmI2c has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmI2c : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmI2c : Cleaning up")
        return True
