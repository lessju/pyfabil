from __future__ import division
__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
import logging
import socket
import time


class MultipleChannelTx(FirmwareBlock):
    """ MultipleChannelTx plugin  """

    @compatibleboards(BoardMake.TpmBoard, BoardMake.Tpm16Board)
    @friendlyname('multiple_channel_tx')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ MultipleChannelTx initialiser
        :param board: Pointer to board instance
        """
        super(MultipleChannelTx, self).__init__(board)

        if 'device' not in list(kwargs.keys()):
            raise PluginError("MultipleChannelTx: Require a node instance")
        self._device = kwargs['device']

        # if 'core' not in list(kwargs.keys()):
        #     raise PluginError("MultipleChannelTx: core_id required")

        if self._device == Device.FPGA_1:
            self._device = 'fpga1'
        elif self._device == Device.FPGA_2:
            self._device = 'fpga2'
        else:
            raise PluginError("MultipleChannelTx: Invalid device %s" % self._device)

        # self._core = kwargs['core']

    #######################################################################################
    def set_instance(self, instance_id, channel_id, destination_id):
        self.board['%s.lmc_channel_gen_multiple.instance_id' % self._device] = instance_id
        self.board['%s.lmc_channel_gen_multiple.destination_id' % self._device] = destination_id
        self.board['%s.lmc_channel_gen_multiple.channel_id' % self._device] = channel_id

    def start(self, instances, timestamp):
        self.board['%s.lmc_channel_gen_multiple.timestamp_request' % self._device] = timestamp
        self.board['%s.lmc_channel_gen_multiple.enable_0' % self._device] = instances & 0xFFFFFFFF
        self.board['%s.lmc_channel_gen_multiple.enable_1' % self._device] = (instances & 0xFFFFFFFF00000000) >> 32
        self.board['%s.lmc_channel_gen_multiple.request' % self._device] = 1

    def stop(self):
        self.board['%s.lmc_channel_gen_multiple.enable_0' % self._device] = 0
        self.board['%s.lmc_channel_gen_multiple.enable_1' % self._device] = 0
        while self.board['%s.lmc_channel_gen_multiple.request' % self._device] == 1:
            time.sleep(0.1)

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise MultipleChannelTx """
        logging.info("MultipleChannelTx has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("MultipleChannelTx: Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("MultipleChannelTx: Cleaning up")
        return True
