from __future__ import division
from builtins import range
__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from time import sleep
import logging
import time


class TpmAda(FirmwareBlock):
    """ TpmAdc tests class """

    @compatibleboards(BoardMake.TpmBoard)
    @friendlyname('tpm_ada')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmAdc initialiser
        :param board: Pointer to board instance
        """
        super(TpmAda, self).__init__(board)

    #######################################################################################

    def initialise_adas(self):
        """ Initialise ADAs on board """
        self.board["board.regfile.ctrl.en_sw_ada_vdd"] = 0x1
        if self.board['board.regfile.date_code'] >= 0x19041700:  # First CPLD FW implementing ADA SPI control
            self.spi = True
            self.ada_ctrl = 0x1
        else:
            self.spi = False
            self.ada_ctrl = 0x5
        self.board['board.regfile.ada_ctrl'] = self.ada_ctrl

    def get_spi_en(self, id):
        if id % 2 == 0:
            id += 1
        else:
            id -= 1
        return (id % 8) + 4

    def get_spi_clk(self, id):
        if id % 2 == 0:
            id += 1
        else:
            id -= 1
        return id // 8 + 4

    def disable_adas(self):
        """ Turn off ADAs """
        self.board["board.regfile.ctrl.en_sw_ada_vdd"] = 0x0

    def set_ada_gain(self, gain):
        """ Set ADA gain
        :param gain: Gain
        """

        # Check if gain is within range
        if not (-6 <= gain <= 15):
            logging.warning("TpmAda: set_ada_gain. Invalid gain (%d), must be between -6 and 15 inclusive. Not setting gain." % gain)
            return

        ada_gain = 15 - gain

        if self.spi:
            for n in range(32):
                self.set_ada_gain_spi(gain, n)
        else:
            self.board["board.regfile.ada_ctrl"] = (ada_gain << 4) | self.ada_ctrl

    def set_ada_gain_spi(self, gain, id):

        # Check if gain is within range
        if not (-6 <= gain <= 15):
            logging.warning("TpmAda: set_ada_gain. Invalid gain (%d), must be between -6 and 15 inclusive. Not setting gain." % gain)
            return

        ada_gain = 15 - gain

        if self.spi:
            spi_request = [ada_gain << 8, 0, 0, 1 << self.get_spi_en(id), 1 << self.get_spi_clk(id), 0x05]  # issue write request with 8 bits flag
            while self.board['board.spi.cmd.start'] == 1:
                time.sleep(0.1)
            self.board['board.spi'] = spi_request
            while self.board['board.spi.cmd.start'] == 1:
                time.sleep(0.1)
        else:
            logging.warning("TpmAda: set_ada_gain_spi is not supported by this TPM. Not setting gain." % gain)

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmPll """
        logging.info("TpmAdc has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """

        logging.info("TpmAdc : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmAdc : Cleaning up")
        return True
