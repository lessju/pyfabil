from builtins import str
import socket

__author__ = 'chiello'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
import logging


class TpmSideChannel(FirmwareBlock):
    """ TpmSideChannel plugin  """

    @compatibleboards(BoardMake.TpmBoard, BoardMake.Tpm16Board)
    @friendlyname('tpm_side_channel')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ TpmSideChannel initialiser
        :param board: Pointer to board instance
        """
        super(TpmSideChannel, self).__init__(board)

        if 'device' not in list(kwargs.keys()):
            raise PluginError("TpmSideChannel: Require a node instance")
        self._device = kwargs['device']

        if self._device == Device.FPGA_1:
            self._device = 'fpga1'
        elif self._device == Device.FPGA_2:
            self._device = 'fpga2'

        self._spectrum_type = "channel"

    #######################################################################################

    def initialise_core(self, spectrum_type="channel"):
        """ Initialise TpmSideChannel core """

        logging.info("Initialising %s side channel" % self._device)

        self._spectrum_type = spectrum_type

        self.board['%s.side_channel_integrator.enable' % self._device] = 0
        # integration time 1s
        self.board['%s.side_channel_integrator.integration_length' % self._device] = 1080000
        self.board['%s.side_channel_integrator.scaling_factor' % self._device] = 14
        self.board['%s.side_channel_integrator.start_read_channel' % self._device] = 0
        self.board['%s.side_channel_integrator.single_ch_sel' % self._device] = 0
        if self._spectrum_type == "channel":
            self.board['%s.side_channel_integrator.last_read_channel' % self._device] = 255
            self.board['%s.side_channel_integrator.single_ch_sel' % self._device] = 0
        else:
            self.board['%s.side_channel_integrator.last_read_channel' % self._device] = 127
            self.board['%s.side_channel_integrator.single_ch_sel' % self._device] = 16

        self.board['%s.side_channel_integrator.enable' % self._device] = 0

    def select_input(self, input_id):
        if self._spectrum_type == "channel":
            self.board['%s.side_channel_integrator.single_ch_sel' % self._device] = input_id
            logging.info("%s side channel streaming input %s: " % (self._device, str(input_id)))
        else:
            logging.info("%s side channel is streaming beam data. It is not possible to switch input!" % self._device)

    def disable(self):
        logging.info("Disabling %s side channel" % self._device)
        self.board['%s.side_channel_integrator.enable' % self._device] = 0

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmSideChannel """
        logging.info("TpmSideChannel has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmSideChannel : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmSideChannel : Cleaning up")
        return True
