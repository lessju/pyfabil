from __future__ import division
from builtins import str
from builtins import hex
from builtins import range
__author__ = 'lessju'

import pyfabil.boards.tpm_hw_definitions as tpm_hw_definitions
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from pyfabil.plugins.firmwareblock import FirmwareBlock

import logging
import time

class TpmFpga(FirmwareBlock):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.TpmBoard,BoardMake.Tpm16Board)
    @friendlyname('tpm_fpga')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmFpga initialiser
        :param board: Pointer to board instance
        """
        super(TpmFpga, self).__init__(board)

        self._board_type = kwargs.get('board_type', 'XTPM')

        if 'device' not in list(kwargs.keys()):
            raise PluginError("TpmFpga: Require a node instance")
        self._device = kwargs['device']

        if self._device == Device.FPGA_1:
            self._device = 'fpga1'
        elif self._device == Device.FPGA_2:
            self._device = 'fpga2'
        else:
            raise PluginError("TpmFpga: Invalid device %s" % self._device)

        self._nof_inputs = 16

        self._fpga_firmware = None
        if self.board.is_programmed(0):
            self._fpga_firmware = self.board['fpga1.regfile.rev']

        # On TPM 1.6 check if we are using the special firmware supporting 800 MHz clock from PLL
        self._config_800MHz = False
        if self.board.hw_rev >= tpm_hw_definitions.TPM_HW_REV_1_5 and self._fpga_firmware is not None:
            if self._fpga_firmware == tpm_hw_definitions.FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5:
                self._config_800MHz = True
                logging.info("FPGA FIRMWARE VERSION: %s" % hex(self._fpga_firmware))
                logging.info("Using 800 MHz FPGA clock exception!")

    #######################################################################################

    def fpga_start(self, input_list=range(16), enabled_list=range(16)):
        """ Set up FPGA
        :param input_list: List of channel to enable
        :param enabled_list:
        :return:
        """

        self.board['%s.jesd204_if.regfile_ctrl.reset_n' % self._device] = 0x0
        self.board['%s.jesd204_if.regfile_ctrl.reset_n' % self._device] = 0x1

        if do_until_eq(lambda : self.board['%s.jesd204_if.regfile_status.qpll_locked' % self._device], 1, ms_retry=100, s_timeout=10) is None:
            logging.fatal("QPLL not locked")
            return

    def fpga_stop(self):
        """ Stop FPGA acquisition and data downloading through 1Gbit Ethernet """
        self.board['%s.jesd204_if.regfile_ctrl' % self._device] = 0x0
        time.sleep(1)

    def fpga_global_reset(self):
        """ Reset FPGA """
        try:
            self.board["%s.regfile.reset.global_rst" % self._device] = 0x1
            self.board["%s.regfile.reset.global_rst" % self._device] = 0x0
        except BoardError:
            pass  # We won't receive a reply for this command

    def fpga_reset(self):
        """ Reset FPGA """
        self.board["%s.regfile.reset" % self._device] = 0xFFFFFFFE
        self.board["%s.regfile.reset" % self._device] = 0x0

    def fpga_apply_sync_delay(self, delay):
        """ Apply synchronous operation delay """
        self.board["%s.pps_manager.timestamp_req_val" % self._device] = delay

    def fpga_mmcm_start(self):
        self.board["%s.drp_jesd_mmcm.reg_23" % self._device] = 0x3

        while True:
            rd = self.board["%s.drp_jesd_mmcm.status.lock" % self._device]
            if rd == 0x1:
                break
            time.sleep(0.01)

    def fpga_mmcm_config(self, freq, custom_config=None):
        # if custom configuration is used
        if custom_config is not None:
            for n in range(23):
                self.board["%s.drp_jesd_mmcm.reg_%d" % (self._device, n)] = custom_config[n]
            self.fpga_mmcm_start()
            logging.debug(self._device.upper() + " MMCM configured with custom configuration")
            return

        # if 800 MHz firmware exception is used, skip configuration
        if self._config_800MHz:
            logging.warning("TpmFpga: FPGA FIRMWARE VERSION: %s"%hex(tpm_hw_definitions.FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5))
            logging.warning("TpmFpga: fpga_mmcm_config method disabled [experimental] [exception for fw %s only]"%hex(tpm_hw_definitions.FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5))
            return

        # Get the clock manager version implmented in the firmware
        if self.board.has_register('%s.regfile.feature_extended.clock_manager_version' % self._device):
            clock_manager_version = self.board['%s.regfile.feature_extended.clock_manager_version' % self._device]
        else:
            clock_manager_version = 1

        if clock_manager_version == 2:
            if freq == 350e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] =  0xFFFF #0xFFFF#0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] =  0x1208 #0x1104#0x1041
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] =  0x0000 #0x0000#0x4800
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] =  0x1208 #0x1104#0x1208
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] =  0x0100 #0x0100#0x0000
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] =  0x11c7 #0x10c4#0x11c7
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] =  0x0000 #0x0080#0x0000
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] =  0x1104 #0x1082#0x1105
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] =  0x0000 #0x0000#0x0080
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] =  0x1e38 #0x171c#0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x0100 #0x0100#0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041 #0x1041#0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0 #0x00c0#0xc0c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041 #0x1041#0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0 #0x00c0#0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041 #0x1041#0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x11c7 #0x10c4#0x11c7
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000 #0x0080#0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x02bc #0x03e8#0x02bc
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7c01 #0x4c01#0x7c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x7fe9 #0x4fe9#0x7fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800 #0x0800#0x0900
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x1190 #0x1990#0x8890
            elif freq == 400e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x4800
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1105
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0xc0c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x02bc
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x7fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0900
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x8890
            elif freq == 700e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x4800
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1104
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1105
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0xc0c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x02bc
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x7fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0900
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x8890
            elif freq == 800e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x4800
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x10c4
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1105
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0xc0c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x11c7
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x02bc
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x7fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0900
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x8890
            elif freq == 1000e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x6800
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x10c3
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x11c8
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0xe0c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x11c8
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x028a
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x7fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x1000
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x1890

            else:
                raise PluginError("TpmFpga: Invalid frequency %s Hz" % str(freq))
        else:
            if freq == 350e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF  #FFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x10c4  #1104
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x0080  #0000
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1104  #1104
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0100  #0100# This bit enable fine phase shift on the clock output
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x171c  #10c4
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0100  #0080# This bit enable fine phase shift on the clock output
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1041  #1082
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x00c0  #0000
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041  #171c
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0 #0000
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041 #1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0 #01c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041 #1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0 #00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041 #1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x10c4 #10c4
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0080 #0080
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x03e8 #03e8
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x4c01 #4c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x4fe9 #4fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800 #0800
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x1990 #1990
            elif freq == 400e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x1514
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x03e8
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7001
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x73e9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x9190
            elif freq == 700e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x10c4
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1082
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x138e
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x10c4
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0080
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x03e8
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x4c01
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x4fe9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x1990
            elif freq == 800e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1083
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0180
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x1514
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x03e8
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7001
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x73e9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x9190
            elif freq == 1000e6:
                self.board["%s.drp_jesd_mmcm.reg_0" % self._device] = 0xFFFF
                self.board["%s.drp_jesd_mmcm.reg_1" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_2" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_3" % self._device] = 0x1082
                self.board["%s.drp_jesd_mmcm.reg_4" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_5" % self._device] = 0x1514
                self.board["%s.drp_jesd_mmcm.reg_6" % self._device] = 0x0100
                self.board["%s.drp_jesd_mmcm.reg_7" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_8" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_9" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_10" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_11" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_12" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_13" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_14" % self._device] = 0x00c0
                self.board["%s.drp_jesd_mmcm.reg_15" % self._device] = 0x1041
                self.board["%s.drp_jesd_mmcm.reg_16" % self._device] = 0x1145
                self.board["%s.drp_jesd_mmcm.reg_17" % self._device] = 0x0000
                self.board["%s.drp_jesd_mmcm.reg_18" % self._device] = 0x03e8
                self.board["%s.drp_jesd_mmcm.reg_19" % self._device] = 0x7001
                self.board["%s.drp_jesd_mmcm.reg_20" % self._device] = 0x73e9
                self.board["%s.drp_jesd_mmcm.reg_21" % self._device] = 0x0800
                self.board["%s.drp_jesd_mmcm.reg_22" % self._device] = 0x9190
            else:
                raise PluginError("TpmFpga: Invalid frequency %s Hz" % str(freq))

        self.fpga_mmcm_start()

        logging.debug(self._device.upper() + " MMCM configured at " + str(freq / 1e6) + " MHz")

    def fpga_jesd_gth_config(self, freq=None):
        if self._config_800MHz:
            logging.warning("TpmFpga: FPGA FIRMWARE VERSION: %s"%hex(tpm_hw_definitions.FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5))
            logging.warning("TpmFpga: fpga_jesd_gth_config method disabled [experimental] [exception for fw %s only]"%hex(tpm_hw_definitions.FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5))
            return
        # GTH DRP register configuration has been read-back from GTH after compiling and running bitfile with different
        # default frequency configurations. Only changing registers in different frequencies configurations are modified.
        #
        # GTH Common DRP registers configuration
        if freq is None:
            logging.warning(self._device.upper() + " GTH using default configuration")
            return

        supported_frequency = [400e6, 700e6, 800e6, 1000e6]

        if freq not in supported_frequency:
            logging.warning(self._device.upper() + " GTH configuration not modified as requested frequency " + str(freq) + " MHz is not supported. Using default configuration")
            return

        frequency_idx = supported_frequency.index(freq)

        # GTH Common DRP registers configuration
        drp_addr = [18, 20, 22, 25, 29, 48]
        drp_data = [[0x25e8, 0x9e, 0x1f,  0x3f4, 0x25e8, 0x1b,],     # 400e6
                    [0x21e8, 0x8a, 0x1ff, 0x3fc, 0x21e8, 0x1b,],     # 700e6
                    [0x25e8, 0x9e, 0x1f,  0x3f4, 0x25e8, 0x1b,],     # 800e6
                    [0x21e8, 0x62, 0x1ff, 0x3fc, 0x21e8, 0x0, ],]    # 1000e6

        drp_data_sel = drp_data[frequency_idx]

        for i in range(4):
            try:
                jesd_gth_drp_base_addr = self.board.memory_map["%s.drp.gth_common_%d" % (self._device, i)].address
            except:
                jesd_gth_drp_base_addr = self.board.memory_map["%s.jesd_drp.gth_common_%d" % (self._device, i)].address
            for a in range(len(drp_addr)):
                self.board[int(jesd_gth_drp_base_addr + 4*drp_addr[a])] = drp_data_sel[a]

        # GTH Channel DRP registers configuration
        drp_addr = [16, 82, 99, 102, 124, 149, 156, 157, 173, 188]
        drp_data = [[0x746, 0x2,   0x80c2, 0xb0f9, 0x2e0, 0xf800, 0x0  , 0x0   , 0x1900, 0xf007,],
                    [0x756, 0x2,   0x80c1, 0xb0f9, 0x1e0, 0xf000, 0x0  , 0x0   , 0x1900, 0xf007,],
                    [0x756, 0x2,   0x80c1, 0xb0f9, 0x1e0, 0xf800, 0x0  , 0x0   , 0x1f00, 0xf007,],
                    [0x766, 0x402, 0x80c0, 0xb0fd, 0xe0 , 0xf000, 0xaac, 0x5568, 0x1f00, 0x7   ,],]

        drp_data_sel = drp_data[frequency_idx]

        for i in range(16):
            try:
                jesd_gth_drp_base_addr = self.board.memory_map["%s.drp.gth_channel_%d" % (self._device, i)].address
            except:
                jesd_gth_drp_base_addr = self.board.memory_map["%s.jesd_drp.gth_channel_%d" % (self._device, i)].address
            for a in range(len(drp_addr)):
                self.board[int(jesd_gth_drp_base_addr + 4*drp_addr[a])] = drp_data_sel[a]

    def fpga_align_adc_clk(self, vco_freq):
        self.board['%s.pps_manager.sync_tc_adc_clk' % self._device] = 0x0007
        time.sleep(0.1)
        while self.board['%s.pps_manager.sync_phase.cnt_adc_clock' % self._device] != 0xFF:
            self.fpga_mmcm_phase_shift(vco_freq)
        rd = hex(self.board['%s.pps_manager.sync_phase.cnt_adc_clock' % self._device])
        logging.debug('%s ADC CLOCK Phase %s' % (self._device.upper(), rd))

    def fpga_mmcm_phase_shift(self, vco_freq):
        vco_divider = 56.0
        single_step_delay = (1.0 / vco_freq) / vco_divider
        steps = 10.0e-9 / single_step_delay  # 10 ns steps
        for n in range(int(steps)):
            self.board['%s.regfile.mmcm_phase_shift_control' % self._device] = 0x3
            while True:
                if self.board['%s.regfile.mmcm_phase_shift_control.en' % self._device] == 0:
                    break

    ##################### Superclass method implementations ###############################

    def initialise(self):
        """ Initialise TpmFpga """
        logging.info("TpmFpga has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.debug("TpmFpga : Checking status")

        # Check FPGA QPLL is locked and receiving data from the FPGA
        if self.board['%s.jesd204_if.regfile_status.qpll_locked' % self._device] == 0x1 and \
           self.board['%s.jesd204_if.regfile_status.valid' % self._device] == 0x1:
            logging.debug('FPGA %s already initialised' % self._device)
            return Status.OK
        else:
            return Status.BoardError

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmFpga : Cleaning up")
        return True
