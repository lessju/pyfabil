from __future__ import division
from builtins import range
__author__ = 'chiello'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
import numpy as np
import logging
import time


class FastDetectStatistics(FirmwareBlock):
    """ FastDetectStatistics plugin  """

    @compatibleboards(BoardMake.TpmBoard, BoardMake.Tpm16Board)
    @friendlyname('fast_detect_statistics')
    @maxinstances(2)
    def __init__(self, board, fsample=800e6, samples_per_frame=864, **kwargs):
        """ FastDetectStatistics initializer
        :param board: Pointer to board instance
        """
        super(FastDetectStatistics, self).__init__(board)

        if 'device' not in list(kwargs.keys()):
            raise PluginError("FastDetectStatistics: Require a node instance")
        self._device = kwargs['device']

        if self._device == Device.FPGA_1:
            self._device = 'fpga1'
        elif self._device == Device.FPGA_2:
            self._device = 'fpga2'
        else:
            raise PluginError("FastDetectStatistics: Invalid device %s" % self._device)

        self._fsample = fsample
        self._sample_period = 1.0 / fsample
        self._samples_per_frame = samples_per_frame
        self._frame_time = self._samples_per_frame * self._sample_period

        self._nof_frames = self.board['%s.fast_detect_statistics.nof_frames' % self._device]
        self._integration_time = self._nof_frames * self._frame_time
        self._integrated_samples = self._nof_frames * self._samples_per_frame

    #######################################################################################

    def set_thresholds(self, upper_threshold, lower_threshold, dwell_samples):
        for adc in self.board.tpm_adc:
            adc.adc_set_fast_detect(upper_threshold, lower_threshold, dwell_samples)

    def set_integration_time(self, integration_time):
        self._nof_frames = int(integration_time // self._frame_time)
        self.board['%s.fast_detect_statistics.nof_frames' % self._device] = self._nof_frames - 1
        self._integration_time = self._nof_frames * self._frame_time
        self._integrated_samples = self._nof_frames * self._samples_per_frame
        return self._integration_time

    def wait(self):
        rd = self.board['%s.fast_detect_statistics.toggle' % self._device]
        while rd == self.board['%s.fast_detect_statistics.toggle' % self._device]:
            time.sleep(0.01)

    def get_level(self):
        return self.board['%s.fast_detect_statistics.level_counters' % self._device]

    def get_event(self):
        return self.board['%s.fast_detect_statistics.event_counters' % self._device]

    ##################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise FastDetectStatistics """
        logging.debug("FastDetectStatistics has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.debug("FastDetectStatistics : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.debug("FastDetectStatistics : Cleaning up")
        return True
