__author__ = 'Alessio Magro'

# Helper to reduces import names

# Plugin Superclass
from pyfabil.plugins.firmwareblock import FirmwareBlock

# TPM plugins
from pyfabil.plugins.tpm.firmware_information import TpmFirmwareInformation
from pyfabil.plugins.tpm.pattern_generator import TpmPatternGenerator
from pyfabil.plugins.tpm.test_generator import TpmTestGenerator
from pyfabil.plugins.tpm.adc_power_meter_simple import AdcPowerMeterSimple
from pyfabil.plugins.tpm.fast_detect_statistics import FastDetectStatistics
from pyfabil.plugins.tpm.antenna_buffer import AntennaBuffer
from pyfabil.plugins.tpm.adc_power_meter import AdcPowerMeter
from pyfabil.plugins.tpm.spead_tx_gen import SpeadTxGen
from pyfabil.plugins.tpm.side_channel import TpmSideChannel
from pyfabil.plugins.tpm.forty_g_xg_core import TpmFortyGCoreXg
from pyfabil.plugins.tpm.ten_g_xg_core import TpmTenGCoreXg
from pyfabil.plugins.tpm.ten_g_core import TpmTenGCore
from pyfabil.plugins.tpm.polyfilter_ox import PolyFilter
from pyfabil.plugins.tpm.beamf_simple import BeamfSimple
from pyfabil.plugins.tpm.beamf_fd import BeamfFD
from pyfabil.plugins.tpm.multiple_channel_tx import MultipleChannelTx
from pyfabil.plugins.tpm.station_beamf import StationBeamformer
from pyfabil.plugins.tpm.integrator import TpmIntegrator
from pyfabil.plugins.tpm.sysmon import TpmSysmon
from pyfabil.plugins.tpm.clock_monitor import TpmClockmon
from pyfabil.plugins.tpm.preadu import PreAdu
from pyfabil.plugins.tpm.f2f import TpmFpga2Fpga
from pyfabil.plugins.tpm.cpld import TpmCpld
from pyfabil.plugins.tpm.fpga import TpmFpga
from pyfabil.plugins.tpm.jesd import TpmJesd
from pyfabil.plugins.tpm.lasc import TpmLasc
from pyfabil.plugins.tpm.i2c import TpmI2c
from pyfabil.plugins.tpm.pll import TpmPll
from pyfabil.plugins.tpm.adc import TpmAdc
from pyfabil.plugins.tpm.ada import TpmAda

# TPM 1.6 plugins
from pyfabil.plugins.tpm_1_6.progflash import Tpm_1_6_ProgFlash
from pyfabil.plugins.tpm_1_6.adc_ad9695 import TpmAdc9695
from pyfabil.plugins.tpm_1_6.f2f_aurora import TpmFpga2FpgaAurora
from pyfabil.plugins.tpm_1_6.cpld import Tpm_1_6_Cpld
from pyfabil.plugins.tpm_1_6.pll import Tpm_1_6_Pll
from pyfabil.plugins.tpm_1_6.mcu import Tpm_1_6_Mcu
from pyfabil.plugins.tpm_1_6.eep import Tpm_1_6_EEP
from pyfabil.plugins.tpm_1_6.preadu import Tpm_1_6_PreAdu
from pyfabil.plugins.tpm_1_6.sysmon import Tpm_1_6_Sysmon
from pyfabil.plugins.tpm_1_6.qsfp_adapter import Tpm_1_6_QSFPAdapter
