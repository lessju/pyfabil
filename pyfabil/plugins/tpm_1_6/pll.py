from __future__ import division

__author__ = 'lessju'

import logging
import time

from pyfabil.base.definitions import *
from pyfabil.plugins.tpm.pll import TpmPll


class Tpm_1_6_Pll(TpmPll):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.Tpm16Board)
    @friendlyname('tpm_pll')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmPll initialiser
        :param board: Pointer to board instance
        """
        super(Tpm_1_6_Pll, self).__init__(board)

    def pll_reset(self):
        """ Perform the PLL reset
        """
        if self.board["board.regfile.enable.adc"] == 0:
            logging.info("ADCs power disabled. Enabling")
            self.board["board.regfile.enable.adc"] = 1
            time.sleep(0.1)
        else:
            logging.info("ADCs power already enabled.")
        if self.board["board.regfile.enable.sysr"] == 0:
            logging.info("SYS_REF power disabled. Enabling")
            self.board["board.regfile.enable.sysr"] = 1
            time.sleep(0.1)
        else:
            logging.info("SYS_REF power already enabled.")
        self.board['board.regfile.pll.resetn'] = 1
        self.board['board.regfile.pll.resetn'] = 0
        time.sleep(0.2)
        self.board['board.regfile.pll.resetn'] = 1
        time.sleep(0.2)

    #################### Superclass method implementations #################################

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmPll : Checking status")

        if self.board[('pll', 0x508)] not in [0xF2, 0xE7]:
            logging.error('TpmPLL: PLL not initialised')
            return Status.BoardError
        if self._pll_status_enabled:
            if self.get_pll_loss_of_lock() != 0:
                logging.error('TpmPLL: PLL Loss of Lock Detected')
                return Status.BoardError

        return Status.OK
