from __future__ import division
from builtins import str
from builtins import hex
from builtins import range
import socket

__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.plugins.tpm.ten_g_xg_core import TpmTenGCoreXg
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
import logging


class TpmFpga2FpgaAurora(FirmwareBlock):
    """ TpmFpga2FpgaAurora plugin  """

    @compatibleboards(BoardMake.Tpm16Board)
    @friendlyname('tpm_f2f')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ TpmFpga2FpgaAurora initialiser
        :param board: Pointer to board instance
        """
        super(TpmFpga2FpgaAurora, self).__init__(board)

        if 'device' not in list(kwargs.keys()):
            raise PluginError("TpmFpga2FpgaAurora: device required")

        # if 'core' not in list(kwargs.keys()):
        #     raise PluginError("TpmFpga2FpgaAurora: core_id required")

        self._fpga = 'fpga1' if kwargs['device'] == Device.FPGA_1 else 'fpga2'

    #######################################################################################

    def assert_reset(self):
        """ Assert reset F2F core """
        self.board['%s.f2f_aurora.ctrl' % self._fpga] = 0x3

    def deassert_reset(self):
        """ Assert reset F2F core """
        self.board['%s.f2f_aurora.ctrl' % self._fpga] = 0x0

    def check_channel_up(self):
        return self.board['%s.f2f_aurora.status.channel_up' % self._fpga]

    def start_tx_test(self):
        self.board['%s.f2f_aurora.test_ctrl.enable_tx' % self._fpga] = 0x1

    def start_rx_test(self):
        self.board['%s.f2f_aurora.test_ctrl.enable_rx' % self._fpga] = 0x1

    def stop_test(self):
        self.board['%s.f2f_aurora.test_ctrl.enable_tx' % self._fpga] = 0x0
        self.board['%s.f2f_aurora.test_ctrl.enable_rx' % self._fpga] = 0x0

    def get_test_result(self, logging_enable=True):
        rd = self.board['%s.f2f_aurora.test_status' % self._fpga]
        if logging_enable:
            logging.info("TpmFpga2FpgaAurora Test %s result" % self._fpga.upper())
            logging.info("    Active: " + str(self.board['%s.f2f_aurora.test_status.test_active' % self._fpga]))
            logging.info("    Errors Detected: " + str(self.board['%s.f2f_aurora.test_status.error_detected'
                                                                  % self._fpga]))
            logging.info("    Lane with Errors: " + hex(self.board['%s.f2f_aurora.test_status.error_lane'
                                                                   % self._fpga]))
        return rd
    
    def check_pll_lock_status(self, show_result=True):
        lock = self.board[f'{self._fpga}.f2f_aurora.status.pll_lock'] > 0
        count = self.board[f'{self._fpga}.f2f_aurora.status.pll_lock_loss_cnt']
        if show_result:
            logging.info(f'{self._fpga.upper()} pll lock loss count {count}')
        return (lock, count)

    def clear_pll_lock_loss_counter(self):
        self.board[f'{self._fpga}.f2f_aurora.status.pll_lock_loss_cnt_reset'] = 1
        return

    def get_soft_err(self):
        return self.board[f'{self._fpga}.f2f_aurora.status.soft_err']
    
    def get_hard_err(self):
        return self.board[f'{self._fpga}.f2f_aurora.status.hard_err']
    # #################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmFpga2FpgaAurora """
        logging.info("TpmFpga2FpgaAurora has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmFpga2FpgaAurora : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmFpga2FpgaAurora : Cleaning up")
        return True
