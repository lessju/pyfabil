import logging

from pyfabil.base.definitions import *
from pyfabil.plugins.tpm.cpld import TpmCpld


class Tpm_1_6_Cpld(TpmCpld):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.Tpm16Board)
    @friendlyname('tpm_cpld')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmCpld initialiser
        :param board: Pointer to board instance
        """
        self._cpld_fw_start_add = 0x10000
        self._cpld_fw_date_add = 0x30000000
        self._max_bitstream_size=0x10000*2

        super(TpmCpld, self).__init__(board)

    #######################################################################################
    def get_version(self):
        version = hex(self.board[self._cpld_fw_date_add])
        return version

    def cpld_flash_read(self, bitfile="cpld_dump.bit"):
        """ Read CPLD FLASH """
        return self.board.tpm_progflash.firmwareRead(0, self._cpld_fw_start_add, self._max_bitstream_size, bitfile)

    def cpld_flash_write(self, bitfile):
        """ Write bitfile to CPLD FLASH """
        logging.info("Writing {} to CPLD flash".format(bitfile))
        return self.board.tpm_progflash.firmwareProgram(0, bitfile, self._cpld_fw_start_add)

    def cpld_efb_wr(self, dat):
        raise NotImplementedError("Tpm_1_6_Cpld.cpld_efb_wr not implemented")
