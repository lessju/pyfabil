__author__ = 'Bubs'

import logging

from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from pyfabil.plugins.firmwareblock import FirmwareBlock

FRAM_BA = 0x90000000  # MCU shadow RAM offset

class McuRegisters(object):
    FRAM_MCU_VERSION = 0x00000000
    FRAM_MCU_COMPILE_DATE = 0x00000004
    FRAM_MCU_GIT_HASH = 0x00000008
    FRAM_MCU_GPR_0 = 0x0000000C
    FRAM_MCU_GPR_1 = 0x00000010
    FRAM_MCU_GPR_2 = 0x00000014
    FRAM_MCU_GPR_3 = 0x00000018
    FRAM_MCU_POOLING_INTERVAL = 0x0000001C
    FRAM_MCU_INTERNAL_COMMAND = 0x00000020
    FRAM_MCU_COUNTER = 0x00000024
    FRAM_MCU_COMPLETE_ADC_COUNTER = 0x00000028
    FRAM_MCU_BOOTLOADER_VERSION = 0x0000002C
    FRAM_MCU_BOOTLOADER_COMMANDS = 0x00000030
    FRAM_MCU_STEP = 0x000000038

    FRAM_BOARD_STATUS = 0x000000E0
    FRAM_BOARD_ALARM = 0x000000E4
    FRAM_BOARD_WARNING = 0x000000E8
    FRAM_ALM_ERR_VALUE = 0x000000EC
    FRAM_WARN_ERR_VALUE = 0x000000F0
    FRAM_I2C_UNREACH_ERR = 0x000000F4

    FRAM_POWERGOOD_ERR = 0x000000FA
    FRAM_POWERGOOD = 0x000000FC
    FRAM_ADC_SW_AVDD1 = 0x00000100
    FRAM_ADC_SW_AVDD2 = 0x00000104
    FRAM_ADC_AVDD3 = 0x00000108
    FRAM_ADC_MAN_1V2 = 0x0000010C
    FRAM_ADC_DDR0_VREF = 0x00000110
    FRAM_ADC_DDR1_VREF = 0x00000114
    FRAM_ADC_VM_DRVDD = 0x00000118
    FRAM_ADC_VIN_SCALED = 0x0000011C
    FRAM_ADC_VM_MAN3V3 = 0x00000120
    FRAM_ADC_VM_MAN1V8 = 0x00000124
    FRAM_ADC_MON_5V0 = 0x00000128
    FRAM_ADC_MGT_AV = 0x0000012C
    FRAM_ADC_MGT_AVTT = 0x00000130
    FRAM_ADC_INTERNAL_MCU_TEMP = 0x00000134
    FRAM_BOARD_TEMP = 0x00000138
    FRAM_FPGA0_TEMP = 0x0000013C
    FRAM_FPGA1_TEMP = 0x00000140
    FRAM_FPGA0_FE_CURRENT = 0x00000144
    FRAM_FPGA1_FE_CURRENT = 0x00000148

    FRAM_WARN_ALARM_UPDATE = 0x00000190

    FRAM_WARN_THR_SW_AVDD1 = 0x00000194
    FRAM_WARN_THR_SW_AVDD2 = 0x00000198
    FRAM_WARN_THR_AVDD3 = 0x0000019C
    FRAM_WARN_THR_MAN_1V2 = 0x000001A0
    FRAM_WARN_THR_DDR0_VREF = 0x000001A4
    FRAM_WARN_THR_DDR1_VREF = 0x000001A8
    FRAM_WARN_THR_VM_DRVDD = 0x000001AC
    FRAM_WARN_THR_VIN_SCALED = 0x000001B0
    FRAM_WARN_THR_VM_MAN3V3 = 0x000001B4
    FRAM_WARN_THR_VM_MAN1V8 = 0x000001B8
    FRAM_WARN_THR_MON_5V0 = 0x000001BC
    FRAM_WARN_THR_MGT_AV = 0x000001C0
    FRAM_WARN_THR_MGT_AVTT = 0x000001C4
    FRAM_WARN_THR_INTERNAL_MCU_TEMP = 0x000001C8
    FRAM_WARN_THR_BOARD_TEMP = 0x000001CC
    FRAM_WARN_THR_FPGA0_TEMP = 0x000001D0
    FRAM_WARN_THR_FPGA1_TEMP = 0x000001D4
    FRAM_WARN_THR_FPGA0_FE_CURRENT = 0x000001D8
    FRAM_WARN_THR_FPGA1_FE_CURRENT = 0x000001E0

    FRAM_ALARM_THR_SW_AVDD1 = 0x00000220
    FRAM_ALARM_THR_SW_AVDD2 = 0x00000224
    FRAM_ALARM_THR_AVDD3 = 0x00000228
    FRAM_ALARM_THR_MAN_1V2 = 0x0000022C
    FRAM_ALARM_THR_DDR0_VREF = 0x00000230
    FRAM_ALARM_THR_DDR1_VREF = 0x00000234
    FRAM_ALARM_THR_VM_DRVDD = 0x00000238
    FRAM_ALARM_THR_VIN_SCALED = 0x0000023C
    FRAM_ALARM_THR_VM_MAN3V3 = 0x00000240
    FRAM_ALARM_THR_VM_MAN1V8 = 0x00000244
    FRAM_ALARM_THR_MON_5V0 = 0x00000248
    FRAM_ALARM_THR_MGT_AV = 0x0000024C
    FRAM_ALARM_THR_MGT_AVTT = 0x00000250
    FRAM_ALARM_THR_INTERNAL_MCU_TEMP = 0x00000254
    FRAM_ALARM_THR_BOARD_TEMP = 0x00000258
    FRAM_ALARM_THR_FPGA0_TEMP = 0x0000025C
    FRAM_ALARM_THR_FPGA1_TEMP = 0x00000260
    FRAM_ALARM_THR_FPGA0_FE_CURRENT = 0x00000264
    FRAM_ALARM_THR_FPGA1_FE_CURRENT = 0x00000268

    MCU_ADC_OFFSET = FRAM_ADC_SW_AVDD1
    ALARM_THR_OFFSET = FRAM_ALARM_THR_SW_AVDD1
    TEMPS_OFFSET = FRAM_BOARD_TEMP


McuADCs = {
    "ADC_SW_AVDD1":     {"add": McuRegisters.FRAM_ADC_SW_AVDD1,   "divider": 0},
    "ADC_SW_AVDD2":     {"add": McuRegisters.FRAM_ADC_SW_AVDD2,   "divider": 0},
    "ADC_AVDD3":        {"add": McuRegisters.FRAM_ADC_AVDD3,      "divider": 0},
    "ADC_MAN_1V2":      {"add": McuRegisters.FRAM_ADC_MAN_1V2,    "divider": 0},
    "ADC_DDR0_VREF":    {"add": McuRegisters.FRAM_ADC_DDR0_VREF,  "divider": 0},
    "ADC_DDR1_VREF":    {"add": McuRegisters.FRAM_ADC_DDR1_VREF,  "divider": 0},
    "ADC_VM_DRVDD":     {"add": McuRegisters.FRAM_ADC_VM_DRVDD,   "divider": 0},
    "ADC_VIN_SCALED":   {"add": McuRegisters.FRAM_ADC_VIN_SCALED, "divider": 12.5},
    "ADC_VM_MAN3V3":    {"add": McuRegisters.FRAM_ADC_VM_MAN3V3,  "divider": 3.74782},
    "ADC_VM_MAN1V8":    {"add": McuRegisters.FRAM_ADC_VM_MAN1V8,  "divider": 2.73914},
    "ADC_MON_5V0":      {"add": McuRegisters.FRAM_ADC_MON_5V0,    "divider": 2.739276},
    "ADC_MGT_AV":       {"add": McuRegisters.FRAM_ADC_MGT_AV,     "divider": 0},
    "ADC_MGT_AVTT":     {"add": McuRegisters.FRAM_ADC_MGT_AVTT,   "divider": 0}
}
Warn_Thresholds = {
    "ADC_SW_AVDD1":     {"add": McuRegisters.FRAM_WARN_THR_SW_AVDD1,    "divider": 0},
    "ADC_SW_AVDD2":     {"add": McuRegisters.FRAM_WARN_THR_SW_AVDD2,    "divider": 0},
    "ADC_AVDD3":        {"add": McuRegisters.FRAM_WARN_THR_AVDD3,       "divider": 0},
    "ADC_MAN_1V2":      {"add": McuRegisters.FRAM_WARN_THR_MAN_1V2,     "divider": 0},
    "ADC_DDR0_VREF":    {"add": McuRegisters.FRAM_WARN_THR_DDR0_VREF,   "divider": 0},
    "ADC_DDR1_VREF":    {"add": McuRegisters.FRAM_WARN_THR_DDR1_VREF,   "divider": 0},
    "ADC_VM_DRVDD":     {"add": McuRegisters.FRAM_WARN_THR_VM_DRVDD,    "divider": 0},
    "ADC_VIN_SCALED":   {"add": McuRegisters.FRAM_WARN_THR_VIN_SCALED,  "divider": 12.5},
    "ADC_VM_MAN3V3":    {"add": McuRegisters.FRAM_WARN_THR_VM_MAN3V3,   "divider": 3.74782},
    "ADC_VM_MAN1V8":    {"add": McuRegisters.FRAM_WARN_THR_VM_MAN1V8,   "divider": 2.73914},
    "ADC_MON_5V0":      {"add": McuRegisters.FRAM_WARN_THR_MON_5V0,     "divider": 2.739276},
    "ADC_MGT_AV":       {"add": McuRegisters.FRAM_WARN_THR_MGT_AV,      "divider": 0},
    "ADC_MGT_AVTT":     {"add": McuRegisters.FRAM_WARN_THR_MGT_AVTT,    "divider": 0}
}
Alarm_Thresholds = {
    "ADC_SW_AVDD1":     {"add": McuRegisters.FRAM_ALARM_THR_SW_AVDD1,    "divider": 0},
    "ADC_SW_AVDD2":     {"add": McuRegisters.FRAM_ALARM_THR_SW_AVDD2,    "divider": 0},
    "ADC_AVDD3":        {"add": McuRegisters.FRAM_ALARM_THR_AVDD3,       "divider": 0},
    "ADC_MAN_1V2":      {"add": McuRegisters.FRAM_ALARM_THR_MAN_1V2,     "divider": 0},
    "ADC_DDR0_VREF":    {"add": McuRegisters.FRAM_ALARM_THR_DDR0_VREF,   "divider": 0},
    "ADC_DDR1_VREF":    {"add": McuRegisters.FRAM_ALARM_THR_DDR1_VREF,   "divider": 0},
    "ADC_VM_DRVDD":     {"add": McuRegisters.FRAM_ALARM_THR_VM_DRVDD,    "divider": 0},
    "ADC_VIN_SCALED":   {"add": McuRegisters.FRAM_ALARM_THR_VIN_SCALED,  "divider": 12.5},
    "ADC_VM_MAN3V3":    {"add": McuRegisters.FRAM_ALARM_THR_VM_MAN3V3,   "divider": 3.74782},
    "ADC_VM_MAN1V8":    {"add": McuRegisters.FRAM_ALARM_THR_VM_MAN1V8,   "divider": 2.73914},
    "ADC_MON_5V0":      {"add": McuRegisters.FRAM_ALARM_THR_MON_5V0,     "divider": 2.739276},
    "ADC_MGT_AV":       {"add": McuRegisters.FRAM_ALARM_THR_MGT_AV,      "divider": 0},
    "ADC_MGT_AVTT":     {"add": McuRegisters.FRAM_ALARM_THR_MGT_AVTT,    "divider": 0}
}


class Tpm_1_6_Mcu(FirmwareBlock):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.Tpm16Board)
    @friendlyname('tpm_monitor')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmMcu initialiser
        :param board: Pointer to board instance
        """
        super(Tpm_1_6_Mcu, self).__init__(board)

        self._board_type = kwargs.get('board_type', 'XTPM')

        self.voltage_dict = {
            'VREF_2V5'   : self.get_vref_2v5,
            'MGT_AVCC'   : self.get_voltage_av,
            'MGT_AVTT'   : self.get_voltage_avtt,
            'SW_AVDD1'   : self.get_voltage_avdd1,
            'SW_AVDD2'   : self.get_voltage_avdd2,
            'AVDD3'      : self.get_voltage_avdd3,
            'MAN_1V2'    : self.get_man_1v2,
            'DDR0_VREF'  : self.get_ddr0_vref,
            'DDR1_VREF'  : self.get_ddr1_vref,
            'VM_DRVDD'   : self.get_vm_drvdd,
            'VIN'        : self.get_vin,
            'MON_3V3'    : self.get_mon_3v3,
            'MON_1V8'    : self.get_mon_1v8,
            'MON_5V0'    : self.get_voltage_5v0 
        }

        self.current_dict = {
        }

        self._available_voltages = list(self.voltage_dict.keys())
        self._available_currents = list(self.current_dict.keys())

    #######################################################################################
    def get_version(self):
        """ Get MCU FW Version
        :return version: MCU FW Version (FW Version_Compile_date_InternalCommand)
        """
        version = hex(self.board.read_address(FRAM_BA + McuRegisters.FRAM_MCU_VERSION)) + "_"
        version += hex(self.board.read_address(FRAM_BA + McuRegisters.FRAM_MCU_COMPILE_DATE)) + "_"
        version += hex(self.board.read_address(FRAM_BA + McuRegisters.FRAM_MCU_INTERNAL_COMMAND))
        return version

    def get_available_mcu_adc(self):
        """ Get List of Supplies Mesures Monitored with MCU ADCs, to be used in other methods
        :return mcu_adc_names: List of MCU Measures Names available
        """
        mcu_adc_names = []
        for measure in McuADCs:
            mcu_adc_names.append(measure)
        print(mcu_adc_names)
        return mcu_adc_names

    def get_adc_value(self, adc_measure=None,all_measure=None):
        """ Get single adc_measure current value, or all available measure values and thresholds (mandatory set once of
        parameter)
        :param adc_measure: use if want get only current value of selected measure
        :param all_measure: set to True if want all available measure current value and alarm and thresholds values
        :return values: single selected value, or dictionary with all available measure value and thresholds
        """
        if adc_measure is not None:
            if adc_measure in McuADCs:
                value = self.board.read_address(FRAM_BA+McuADCs[adc_measure]["add"])
                op_status = 0
            else:
                logging.error("Error: measure not found, please use one in available")
                op_status = -1
                value = -1
            return value, op_status
        elif all_measure is not None:
            values = []
            op_status = 0
            for meas in McuADCs:
                print(meas)
                value = self.board.read_address(FRAM_BA+McuADCs[meas]["add"])
                w_tr_reg = self.board.read_address(FRAM_BA+Warn_Thresholds[meas]["add"])
                min_w_tr = w_tr_reg & 0xffff
                max_w_tr = (w_tr_reg & 0xffff0000) >> 16
                a_tr_reg = self.board.read_address(FRAM_BA + Alarm_Thresholds[meas]["add"])
                min_a_tr = a_tr_reg & 0xffff
                max_a_tr = (a_tr_reg & 0xffff0000) >> 16
                data = {meas: {"value": value/1000, "unit": "V", "max_alm": max_a_tr, "min_alm": min_a_tr,
                               "max_wrn": max_w_tr, "min_wrn": min_w_tr}}
                values.append(data)
                op_status = 0
            return values, op_status

    def set_adc_warning_thresholds(self, adc_measure, min_thr, max_thr):
        """ Set slected MCU ADC measure Warning Max and Min thresholds values
        :param adc_measure: name of available measure to be configured
        :param min_thr: minimun warning threshold value in Volt
        :param max_thr: maximun warning threshold value in Volt
        :return op_status: operation status, 0 success, -1 failed
        :note this method not work properly with bios < v0.3.0
        """
        if adc_measure is not None:
            if adc_measure in McuADCs:
                thr_warn_reg = (((max_thr*1000) & 0xffff) << 16) | ((min_thr*1000) & 0xffff)
                self.board.write_address(FRAM_BA + Warn_Thresholds[adc_measure]["add"], thr_warn_reg)
                self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)
                op_status = 0
            else:
                logging.error("Error: measure not found, please use one in available")
                op_status = -1
            return op_status

    def set_adc_alarm_thresholds(self, adc_measure, min_thr, max_thr):
        """ Set slected MCU ADC measure Alarrm Max and Min thresholds values
        :param adc_measure: name of available measure to be configured
        :param min_thr: minimun alarm threshold value in Volt
        :param max_thr: maximun alarm threshold value in Volt
        :return op_status: operation status, 0 success, -1 failed
        :note this method not work properly with bios < v0.3.0
        """
        if adc_measure is not None:
            if adc_measure in McuADCs:
                thr_alm_reg = (((max_thr*1000) & 0xffff) << 16) | ((min_thr*1000) & 0xffff)
                self.board.write_address(FRAM_BA + Alarm_Thresholds[adc_measure]["add"], thr_alm_reg)
                self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)
                op_status = 0
            else:
                logging.error("Error: measure not found, please use one in available")
                op_status = -1
            return op_status

    def get_rounded_adc_value(self, McuADCs_key):
        value = self.get_adc_value(adc_measure=McuADCs_key)[0]
        return round(value/1000, 2)

    def get_voltage_5v0(self):
        return self.get_rounded_adc_value("ADC_MON_5V0")

    def get_voltage_fpga0(self):
        # compatibility with tpm 1.2 LASC plugin
        return None

    def get_voltage_fpga1(self):
        # compatibility with tpm 1.2 LASC plugin
        return None

    def get_voltage_av(self):
        return self.get_rounded_adc_value("ADC_MGT_AV")

    def get_voltage_avtt(self):
        return self.get_rounded_adc_value("ADC_MGT_AVTT")

    def get_voltage_vcc_aux(self):
        # compatibility with tpm 1.2 LASC plugin
        return None

    def get_voltage_avdd1(self):
        return self.get_rounded_adc_value("ADC_SW_AVDD1")

    def get_voltage_avdd2(self):
        return self.get_rounded_adc_value("ADC_SW_AVDD2")

    def get_voltage_avdd3(self):
        return self.get_rounded_adc_value("ADC_AVDD3")

    def get_vref_2v5(self):
        # TODO: add support for External ADC reference
        return None

    def get_man_1v2(self):
        return self.get_rounded_adc_value("ADC_MAN_1V2")
    
    def get_ddr0_vref(self):
        return self.get_rounded_adc_value("ADC_DDR0_VREF")
    
    def get_ddr1_vref(self):
        return self.get_rounded_adc_value("ADC_DDR1_VREF")

    def get_vm_drvdd(self):
        return self.get_rounded_adc_value("ADC_VM_DRVDD")
    
    def get_vin(self):
        return self.get_rounded_adc_value("ADC_VIN_SCALED")
    
    def get_mon_3v3(self):
        return self.get_rounded_adc_value("ADC_VM_MAN3V3")
    
    def get_mon_1v8(self):
        return self.get_rounded_adc_value("ADC_VM_MAN1V8")

    def get_temperature(self):
        """ Get Board temperature measured on PCB temperature sensor
        :return temp_upd: temperature value, if I2C access error is detected a value of -273 will be returned
        :note until ADC PS is off the returned value of temp is 0
        :note this method can not work properly with bios < v0.3.0
        """
        step = 0.0625
        temp = (self.board.read_address(FRAM_BA + McuRegisters.FRAM_BOARD_TEMP)) & 0xffff
        temp_status = (temp & 0xC000) >> 12
        if temp & 0x1000 == 0x1000:
            temp = (temp & 0xfff) - 4096
        else:
            temp = temp & 0xfff
        temp_f = temp * step
        temp_f = round(temp_f, 2)
        if self.board["board.regfile.date_code"] > self.board.MCU_FW_VER_LOCK_I2C_CHANGE:
            if temp_status & 0x8 == 0x8:
                logging.error("Board Temperature not updated for MCU I2C access error")
                # temp_prev = temp_f
                temp_upd = -273
            elif temp_status & 0x4 == 0x4:
                logging.error("Board Temperature not updated, ADC PS is Off for Temp Error")
                # temp_prev = temp_f
                temp_upd = -273
            else:
                # temp_prev = None
                temp_upd = temp_f
        else:
            temp_upd = temp_f
        return temp_upd

    def get_mcu_temperature(self):
        """ Get MCU temperature
        :return temp_f: MCU temperature value
        """
        temp = (self.board.read_address(FRAM_BA + McuRegisters.FRAM_ADC_INTERNAL_MCU_TEMP))
        temp_f = float(temp/10000)
        temp_f = round(temp_f, 2)
        return temp_f

    def set_board_warn_temp_thresholds(self, min_temp, max_temp):
        """ Set board Warning temperature Max and Min thresholds values
        :param min_temp: minimun temp threshold value in Celsius degree
        :param max_temp: maximun temp threshold value in Celsius degree
        :note this method can not work properly with bios < v0.3.0
        """
        tmax = int(max_temp / 0.0625)
        tmin = int(min_temp / 0.0625)
        regval = ((tmax & 0xffff) << 16) | (tmin & 0xffff)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_THR_BOARD_TEMP), regval)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)

    def set_board_alm_temp_thresholds(self, min_temp, max_temp):
        """ Set board Alarm temperature Max and Min thresholds values
        :param min_temp: minimun temp threshold value in Celsius degree
        :param max_temp: maximun temp threshold value in Celsius degree
        :note this method can not work properly with bios < v0.3.0
        """
        tmax = int(max_temp / 0.0625)
        tmin = int(min_temp / 0.0625)
        regval = ((tmax & 0xffff) << 16) | (tmin & 0xffff)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_ALARM_THR_BOARD_TEMP), regval)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)

    def set_fpgas_warn_temp_thresholds(self, min_temp, max_temp, fpga_id):
        """ Set selected FPGA Warning temperature Max and Min thresholds values
        :param min_temp: minimun temp threshold value in Celsius degree
        :param max_temp: maximun temp threshold value in Celsius degree
        :param fpga_id: select which FPGA' temp thresholds will be configured (possible values:0,1)
        :note this method can not work properly with bios < v0.3.0
        """
        max_value = int((max_temp + 273.67777) * 65536 / 501.3743)
        min_value = int((min_temp + 273.67777) * 65536 / 501.3743)
        regval = ((max_value & 0xffff) << 16) | (min_value & 0xffff)
        if fpga_id == 0:
            self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_THR_FPGA0_TEMP), regval)
        else:
            self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_THR_FPGA1_TEMP), regval)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)

    def set_fpgas_alm_temp_thresholds(self, min_temp, max_temp, fpga_id):
        """ Set selected FPGA Alarm temperature Max and Min thresholds values
        :param min_temp: minimun temp threshold value in Celsius degree
        :param max_temp: maximun temp threshold value in Celsius degree
        :param fpga_id: select which FPGA' temp thresholds will be configured (possible values:0,1)
        :note this method can not work properly with bios < v0.3.0
        """
        max_value = int((max_temp + 273.67777) * 65536 / 501.3743)
        min_value = int((min_temp + 273.67777) * 65536 / 501.3743)
        regval = ((max_value & 0xffff) << 16) | (min_value & 0xffff)
        if fpga_id == 0:
            self.board.write_address((FRAM_BA + McuRegisters.FRAM_ALARM_THR_FPGA0_TEMP), regval)
        else:
            self.board.write_address((FRAM_BA + McuRegisters.FRAM_ALARM_THR_FPGA1_TEMP), regval)
        self.board.write_address((FRAM_BA + McuRegisters.FRAM_WARN_ALARM_UPDATE), 0x1)

    def get_fpga_alm_temp_thresholds(self, fpga_id):
        """ Get selected FPGA Alarm temperature Max and Min thresholds values
        :param fpga_id: select which FPGA' temp thresholds will be returned (possible values:0,1)
        :return min_temp: minimum temperature threshold value
        :return man_temp: maximum temperature threshold value
        :note this method can not work properly with bios < v0.3.0
        """
        if fpga_id == 0:
            add = FRAM_BA + McuRegisters.FRAM_ALARM_THR_FPGA0_TEMP
        else:
            add = FRAM_BA + McuRegisters.FRAM_ALARM_THR_FPGA1_TEMP
        fpga0_raw = self.board[add]
        fpga_max = (fpga0_raw & 0xffff0000) >> 16
        fpga_min = (fpga0_raw & 0xffff)
        max_temp = (fpga_max * 501.3743/65536) - 273.67777
        min_temp = (fpga_min * 501.3743/65536) - 273.67777
        max_temp = round(max_temp, 2)
        min_temp = round(min_temp, 2)
        return min_temp, max_temp

    def get_fpga_warn_temp_thresholds(self, fpga_id):
        """ Get selected FPGA Warning temperature Max and Min thresholds values
        :param fpga_id: select which FPGA' temp thresholds will be returned(possible values:0,1)
        :return min_temp: minimum temperature threshold value
        :return man_temp: maximum temperature threshold value
        :note this method can not work properly with bios < v0.3.0
        """
        if fpga_id == 0:
            add = FRAM_BA + McuRegisters.FRAM_WARN_THR_FPGA0_TEMP
        else:
            add = FRAM_BA + McuRegisters.FRAM_WARN_THR_FPGA1_TEMP
        fpga0_raw = self.board[add]
        fpga_max = (fpga0_raw & 0xffff0000) >> 16
        fpga_min = (fpga0_raw & 0xffff)
        max_temp = (fpga_max * 501.3743 / 65536) - 273.67777
        min_temp = (fpga_min * 501.3743 / 65536) - 273.67777
        max_temp = round(max_temp, 2)
        min_temp = round(min_temp, 2)
        return min_temp, max_temp

    def get_board_warn_temp_thresholds(self):
        """ Get board Warning temperature Max and Min thresholds values
        :return min_temp: minimum temperature threshold value
        :return man_temp: maximum temperature threshold value
        :note this method can not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_WARN_THR_BOARD_TEMP
        temp_raw = self.board[add]
        max_temp = ((temp_raw & 0xffff0000) >> 16) * 0.0625
        min_temp = (temp_raw & 0xffff) * 0.0625
        max_temp = round(max_temp, 2)
        min_temp = round(min_temp, 2)
        return min_temp, max_temp

    def get_board_alm_temp_thresholds(self):
        """ Get board Alarm temperature Max and Min thresholds values
        :return min_temp: minimum temperature threshold value
        :return man_temp: maximum temperature threshold value
        :note this method can not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_ALARM_THR_BOARD_TEMP
        temp_raw = self.board[add]
        max_temp = ((temp_raw & 0xffff0000) >> 16) * 0.0625
        min_temp = (temp_raw & 0xffff) * 0.0625
        max_temp = round(max_temp, 2)
        min_temp = round(min_temp, 2)
        return min_temp, max_temp

    def get_mcu_last_exec_phase_id(self):
        """ Get mcu last execution phase id
        :return exec_phaseid: last execution phase id
        """
        add = FRAM_BA + McuRegisters.FRAM_MCU_STEP
        exec_phaseid = self.board[add]
        return exec_phaseid

    def get_volttemp_err_status(self):
        """ get status of voltage or temp error detection by MCU, if error is detected the value
        that has generated the error will be returned
        :return error_status: error status vale if not 0 an error was detected
        :return error_value: value of the measure that caused the error in case of error detection, else 0
        :note this method not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_BOARD_ALARM
        error_status = self.board[add]
        add = FRAM_BA + McuRegisters.FRAM_ALM_ERR_VALUE
        error_value = self.board[add]
        return error_status, error_value

    def get_volttemp_warn_status(self):
        """ get status of voltage or temp warning detection by MCU, if warning is detected the value
        that has generated the warning will be returned
        :return warning_status: error status vale if not 0 an error was detected
        :return warning_value: value of the measure that caused the warning in case of error detection
        :note this method not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_BOARD_WARNING
        warning_status = self.board[add]
        add = FRAM_BA + McuRegisters.FRAM_WARN_ERR_VALUE
        warning_value = self.board[add]
        return warning_status, warning_value


    def get_pgood_err(self):
        """ get status of power good error status
        :return pgood_err: power good error value if not 0 an error was detetcted
        :note this method not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_POWERGOOD_ERR
        pgood_err = self.board[add]
        return pgood_err

    def get_i2c_unreach_err(self):
        """ get status of i2c unreachable error status
        :return i2cunreach_err: i2c unreachable error value if not 0 an error was detetcted
        :note this method not work properly with bios < v0.3.0
        """
        add = FRAM_BA + McuRegisters.FRAM_I2C_UNREACH_ERR
        i2cunreach_err = self.board[add]
        return i2cunreach_err
        
    def get_voltage(self, voltage=None):
        if voltage is None:
            voltages = self._available_voltages
        else:
            if voltage not in self._available_voltages:
                # raise PluginError(f"No voltage named '{voltage}' \n Options are {', '.join(self._available_voltages)}")
                return {}
            voltages = [voltage]
        rt = {}
        for v in voltages:
            rt[v] = self.voltage_dict[v]()
        return rt
    
    def get_current(self, current=None):
        if current is None:
            currents = self._available_currents
        else:
            if current not in self._available_currents:
                # raise PluginError(f"No current named '{current}' \n Options are {', '.join(self._available_currents)}")
                return {}
            currents = [current]
        rt = {}
        for c in currents:
            rt[c] = self.current_dict[c]()
        return rt

    def get_available_voltages(self):
        return self._available_voltages
    
    def get_available_currents(self):
        return self._available_currents

    # #################### Superclass method implementations #################################

    def initialise(self):
        """ Initialise TpmMcu """
        logging.info("TpmMcu has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmMcu : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmMcu : Cleaning up")
        return True
