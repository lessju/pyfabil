from builtins import range

__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from pyfabil.plugins.tpm.preadu import PreAdu
import logging
import time
from copy import copy

class Tpm_1_6_PreAdu(PreAdu):
    """ AdcPowerMeter plugin  """

    @compatibleboards(BoardMake.TpmBoard, BoardMake.Tpm16Board)
    @friendlyname('tpm_preadu')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ AdcPowerMeter initialiser
        :param board: Pointer to board instance
        """
        super(Tpm_1_6_PreAdu, self).__init__(board, **kwargs)

    #######################################################################################
    def powered_on(self):
        return self.board["board.regfile.enable.fe"] > 0

    def switch_on(self):
        """ Switch preadu on """

        # Switch on preadu
        self.board["board.regfile.enable.fe"] = 1
        time.sleep(0.2)

        # Check that preadu has been switched on properly
        value = self.board["board.regfile.enable.fe"]
        if value != 1:
            logging.warning(f"Error! preADU power is not high: {value}")
            return
        logging.debug("preADU power on done!")

        # Reading from EEP disabled for TPM 1.6 for now - Peter 04/07/2023
        # NOTE: tpm_cpld.eep_rd32 will need to be updated to not used tpm_i2c plugin which
        # is TPM 1.2 ONLY
        # Read and load preadu configuration
        # self.eep_read()
        # self.write_configuration()

    def switch_off(self):
        """ Switch preadu off """

        # Switch off preadu
        self.board["board.regfile.enable.fe"] = 0
        time.sleep(0.2)

        # Check that preadu has been switched off properly
        value = self.board["board.regfile.enable.fe"]
        if value == 0x0:
            logging.debug("preADU power off done!")
        else:
            logging.warning(f"Error! preADU power is not low: {value}")

    def set_attenuation(self, attenuation, channels=None):
        """ 
        Set attenuation level for a particular channel.
        Supports attenuation from 0 to 31 with a precision of 0.25.
        :param attenuation: Value of attenuation
        :param channels: Preadu channels (None if to be applied to all channels """

        if channels is None:
            channels = list(range(self._nof_channels))

        # Sanity checks
        if not 0 <= attenuation < 2 ** 5:
            logging.warning(f"Cannot set PREADU attenuation {attenuation}, out of range")
            return

        for channel in channels:
            if not 0 <= channel <= self._nof_channels:
                logging.warning(f"Cannot set attenuation for channel {channel}, invalid channel")

            if attenuation % 0.25 != 0:
                logging.warning(f"PreADU attenuation precision of 0.25 supported. Value {attenuation} specified will be rounded to {attenuation // 0.25 / 4}")
            # Apply attenuation with currently selected filter
            attenuation_int = int(attenuation * 4)
            attenuation_value = 0
            attenuation_value = self.set_bits(input=attenuation_value, bit_range=(1, 7), value=attenuation_int)
            self.channel_filters[channel] = attenuation_value
            logging.debug(f"preADU channel {channel} attenuation has been set to {attenuation_value}")

    def get_attenuation(self):
        """
        Returns the current attenuation value. 
        Stored in channel_filters[7:1] with the 2 least significant bits of which
        forming a fractional component. 
        For example if channel_filters[7:1] == 33 this corresponds to an attenuation
        of  1000.01 (base 2) or 8.25 (base 10)
        """
        output = [None]*self._nof_channels
        for channel_index in range(self._nof_channels):
            output[channel_index] = self.get_bits(input=self.channel_filters[channel_index], bit_range=(1, 7)) * 0.25
        return output

    # Log warning if unsupported methods used
    # Override inherited TPM 1.2 methods
    # -----------------------------------------------------------------------------------
    def disable_channels(self, channels=None):
        logging.warning("disable_channels method is only supported on TPM 1.2")
        return

    def select_low_passband(self):
        logging.warning("select_low_passband method is only supported on TPM 1.2")
        return

    def select_high_passband(self):
        logging.warning("select_high_passband method is only supported on TPM 1.2")
        return

    def enable_channels(self, channels=None):
        logging.warning("enable_channels method is only supported on TPM 1.2")
        return

    def get_passband(self):
        logging.warning("get_passband method is only supported on TPM 1.2")
        return
