from __future__ import division
from builtins import str
__author__ = 'bubs'

from pyfabil.plugins.firmwareblock import FirmwareBlock
import logging
import time

from pyfabil.base.definitions import *
from pyfabil.plugins.tpm.sysmon import TpmSysmon

ad_16_fpga = [
    {"name": "VM_FE0",       "ref": 3.5000, "fpga": 0, "ch": 0,  "divider": 11.5/(31.6+11.5)},
    {"name": "VM_FE1",       "ref": 3.5000, "fpga": 1, "ch": 9,  "divider" : 11.5/(31.6+11.5)},
    {"name": "FE0_mVA",      "ref": 2.5,    "fpga": 0, "ch": 2,  "divider": 0.400, "unit": "A"},  # ACS70311 400 mV/A TYPE
    {"name": "FE1_mVA",      "ref": 2.5,    "fpga": 1, "ch": 8,  "divider": 0.400, "unit": "A"},  # ACS70311 400 mV/A TYPE
    {"name": "VM_DDR0_VTT",  "ref": 0.6000, "fpga": 0, "ch": 3},
    {"name": "VM_AGP0",      "ref": 0.9160, "fpga": 0, "ch": 1},
    {"name": "VM_AGP1",      "ref": 0.9160, "fpga": 0, "ch": 4},
    {"name": "VM_AGP2",      "ref": 0.9160, "fpga": 0, "ch": 7},
    {"name": "VM_AGP3",      "ref": 0.9160, "fpga": 0, "ch": 10},
    {"name": "VM_AGP4",      "ref": 0.9160, "fpga": 1, "ch": 2},
    {"name": "VM_AGP5",      "ref": 1,      "fpga": 1, "ch": 0},
    {"name": "VM_AGP6",      "ref": 0.9160, "fpga": 1, "ch": 3},
    {"name": "VM_AGP7",      "ref": 0.9160, "fpga": 1, "ch": 4},
    {"name": "VM_CLK0B",     "ref": 3.3000, "fpga": 0, "ch": 5,  "divider": 11.5/(31.6+11.5)},
    {"name": "VM_CLK1B",     "ref": 3.3000, "fpga": 1, "ch": 1,  "divider": 11.5/(31.6+11.5)},
    {"name": "VM_MGT0_AUX",  "ref": 1.8000, "fpga": 0, "ch": 6,  "divider": 11.5/(20+11.5)},
    {"name": "VM_MGT1_AUX",  "ref": 1.8000, "fpga": 1, "ch": 5,  "divider": 11.5/(20+11.5)},
    {"name": "VM_ADA0",      "ref": 3.3000, "fpga": 0, "ch": 8,  "divider": 11.5/(31.6+11.5)},
    {"name": "VM_ADA1",      "ref": 3.3000, "fpga": 0, "ch": 9,  "divider": 11.5/(31.6+11.5)},
    {"name": "VM_PLL",       "ref": 3.3000, "fpga": 0, "ch": 11, "divider": 11.5/(31.6+11.5)},
    {"name": "VM_DDR1_VTT",  "ref": 0.6000, "fpga": 1, "ch": 6},
    {"name": "VM_DDR1_VDD",  "ref": 1.2000, "fpga": 1, "ch": 11, "divider": 11/(4.99+11)},
    {"name": "VM_DVDD",      "ref": 1.1000, "fpga": 1, "ch": 7,  "divider": 11/(4.99+11)},
    {"name": "VM_SW_AMP",    "ref": 3.5000, "fpga": 1, "ch": 10, "divider": 4.53/(20+4.53)},
    ]

def get_value_index(name, device):
    index = -1
    if device == "fpga1":
        fpga_ind = 0
    else:
        fpga_ind = 1
    for i in range(0, len(ad_16_fpga)):
        if (name == ad_16_fpga[i]["name"]) and (fpga_ind == ad_16_fpga[i]["fpga"]):
            index = i
            break
    return index


class Tpm_1_6_Sysmon(TpmSysmon):
    """ FirmwareBlock tests class """

    @compatibleboards(BoardMake.Tpm16Board)
    @friendlyname('tpm_sysmon')
    @maxinstances(2)
    def __init__(self, board, **kwargs):
        """ TpmSysmon initialiser
        :param board: Pointer to board instance
        """
        super(Tpm_1_6_Sysmon, self).__init__(board, **kwargs)

    def get_vcc_int(self):
        """ Read internal VCC"""
        val = self.board['%s.sys_mon.vccint' % self._device]
        val = float(val)/65536*3
        return val

    def get_vcc_aux(self):
        """ Read internal VAUX"""
        val = self.board['%s.sys_mon.vccaux' % self._device]
        val = float(val)/65536*3
        return val

    def get_vcc_fe(self):
        if self._device == "fpga1":
            vcc = self.read_adx(ad_16_fpga[0]["ch"])
            if "divider" in ad_16_fpga[0]:
                vcc = vcc/ad_16_fpga[0]["divider"]
        else:
            vcc = self.read_adx(ad_16_fpga[1]["ch"])
            if "divider" in ad_16_fpga[1]:
                vcc = vcc / ad_16_fpga[1]["divider"]
        return vcc

    def get_vm_ddr0_vtt(self):
        vcc = self.read_adx(ad_16_fpga[0]["ch"])
        if "divider" in ad_16_fpga[0]:
            vcc = vcc/ad_16_fpga[0]["divider"]
        return vcc

    def get_available_measure(self):
        measures = []
        if self._device == "fpga1":
            fpga_ind = 0
        else:
            fpga_ind = 1
        for i in range(0, len(ad_16_fpga)):
            if fpga_ind == ad_16_fpga[i]["fpga"]:
                measures.append(ad_16_fpga[i]["name"])
        print(measures)
    
    def get_available_voltages(self):
        voltages = []
        if self._device == "fpga1":
            fpga_ind = 0
        else:
            fpga_ind = 1
        for i in range(0, len(ad_16_fpga)):
            if fpga_ind == ad_16_fpga[i]["fpga"] and ad_16_fpga[i].get("unit") != "A":
                voltages.append(ad_16_fpga[i]["name"])
        return voltages
    
    def get_available_currents(self):
        currents = []
        if self._device == "fpga1":
            fpga_ind = 0
        else:
            fpga_ind = 1
        for i in range(0, len(ad_16_fpga)):
            if fpga_ind == ad_16_fpga[i]["fpga"] and ad_16_fpga[i].get("unit") == "A":
                currents.append(ad_16_fpga[i]["name"])
        return currents


    def get_measure_val(self, name):
        index = get_value_index(name, self._device)
        if index == -1:
            logging.error("ERROR: measure not available check provided name")
            return -1
        else:
            val = self.read_adx(ad_16_fpga[index]["ch"])
            if "divider" in ad_16_fpga[index]:
                val = val / ad_16_fpga[index]["divider"]
            return val

    def get_voltage(self, voltage=None):
        if voltage is None:
            voltages = self.get_available_voltages()
        else:
            if voltage not in self.get_available_voltages():
                # raise PluginError(f"No voltage named '{voltage}' \n Options are {self.get_available_voltages()}")
                return {}
            voltages = [voltage]
        rt = {}
        for name in voltages:
            rt[name] = round(self.get_measure_val(name), 2)
        return rt
    
    def get_current(self, current=None):
        if current is None:
            currents = self.get_available_currents()
        else:
            if current not in self.get_available_currents():
                # raise PluginError(f"No current named '{current}' \n Options are {self.get_available_currents()}")
                return {}
            currents = [current]
        rt = {}
        for name in currents:
            rt[name] = round(self.get_measure_val(name), 2)
        return rt

##################### Superclass method implementations #################################


def initialise(self):
    """ Initialise TpmSysmon """
    logging.info("TpmSysmon has been initialised")
    return True


def status_check(self):
    """ Perform status check
    :return: Status
    """
    logging.info("TpmSysmon : Checking status")
    return Status.OK


def clean_up(self):
    """ Perform cleanup
    :return: Success
    """
    logging.info("TpmSysmon : Cleaning up")
    return True
